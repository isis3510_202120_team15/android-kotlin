package com.example.restaurantorderapp.model

import android.os.Parcel
import android.os.Parcelable

data class Reserva(
    //Actualizado
    val id:String,
    val clienteId:String,
    val restauranteId:String,
    var diaReserva: String,
    var horaReserva: String,
    var platos:Array<String>,
    var puestos:Int
):Parcelable {
    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(p0: Parcel?, p1: Int) {
        p0?.let {
            p0.writeString(id)
            p0.writeString(clienteId)
            p0.writeString(restauranteId)
            p0.writeString(diaReserva)
            p0.writeString(horaReserva)
            p0.writeStringArray(platos)
            p0.writeInt(puestos)
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Reserva

        if (id != other.id) return false
        if (clienteId != other.clienteId) return false
        if (restauranteId != other.restauranteId) return false
        if (diaReserva != other.diaReserva) return false
        if (horaReserva != other.horaReserva) return false
        if (!platos.contentEquals(other.platos)) return false
        if (puestos != other.puestos) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + clienteId.hashCode()
        result = 31 * result + restauranteId.hashCode()
        result = 31 * result + diaReserva.hashCode()
        result = 31 * result + horaReserva.hashCode()
        result = 31 * result + platos.contentHashCode()
        result = 31 * result + puestos
        return result
    }

    companion object {
        @JvmField val CREATOR: Parcelable.Creator<Reserva> = object : Parcelable.Creator<Reserva> {
            override fun newArray(size: Int): Array<Reserva?> = arrayOfNulls(size)

            override fun createFromParcel(source: Parcel): Reserva = Reserva(source)

        }

    }
    constructor(source: Parcel) : this(source.readString().toString(),source.readString().toString(),
        source.readString().toString(),source.readString().toString(), source.readString().toString(),
        source.createStringArray() as Array<String>,source.readInt())


}
