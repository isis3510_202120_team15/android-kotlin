package com.example.restaurantorderapp.model

data class ErrorHandler(val error:String){
    override fun toString(): String {
        return error.replace("[{}\"]".toRegex(), "")
            .replace("error:", "")
    }
}