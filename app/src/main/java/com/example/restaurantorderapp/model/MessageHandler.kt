package com.example.restaurantorderapp.model

data class MessageHandler(val message:String){
    override fun toString(): String {
        return message.replace("[{}\"]".toRegex(), "")
            .replace("error:", "")
    }
}