package com.example.restaurantorderapp.model

import android.os.Parcel
import android.os.Parcelable

data class Plato(
    val id:String,
    val nombre:String,
    val precio:Int
):Parcelable {
    //implementing Parcelable
    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(p0: Parcel?, p1: Int) {
        p0?.let {
            p0.writeString(id)
            p0.writeString(nombre)
            p0.writeInt(precio)
        }
    }
    companion object {
        @JvmField val CREATOR: Parcelable.Creator<Plato> = object : Parcelable.Creator<Plato> {
            override fun newArray(size: Int): Array<Plato?> = arrayOfNulls(size)

            override fun createFromParcel(source: Parcel): Plato = Plato(source)

        }

    }
    constructor(source: Parcel) : this(source.readString().toString(),source.readString().toString(),source.readInt())
}
