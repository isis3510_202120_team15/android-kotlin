package com.example.restaurantorderapp.model

import android.location.Location.distanceBetween
import android.os.Build
import android.os.Parcel
import android.os.Parcelable
import androidx.annotation.RequiresApi
import java.math.RoundingMode
import java.text.DecimalFormat

data class Restaurante(
    val id:String,
    val nombre:String,
    val correo:String,
    val telefono:String,
    val nit:String,
    val direccion:String,
    val pedidos: Array<Reserva>,
    val platos: Array<Plato>,
    val horariosReserva: Array<String>,
    val puestosReserva: Int,
    val disponibleTTG: Boolean,
    val rating: IntArray?,
    val coord: String,
    val foto: String
):Parcelable{
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Restaurante

        if (id != other.id) return false
        if (nombre != other.nombre) return false
        if (correo != other.correo) return false
        if (telefono != other.telefono) return false
        if (nit != other.nit) return false
        if (direccion != other.direccion) return false
        if (!pedidos.contentEquals(other.pedidos)) return false
        if (!platos.contentEquals(other.platos)) return false
        if (!horariosReserva.contentEquals(other.horariosReserva)) return false
        if (puestosReserva != other.puestosReserva) return false
        if (disponibleTTG != other.disponibleTTG) return false
        if (!rating.contentEquals(other.rating)) return false
        if (coord != other.coord)   return false
        if (foto != other.foto)   return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + nombre.hashCode()
        result = 31 * result + correo.hashCode()
        result = 31 * result + telefono.hashCode()
        result = 31 * result + nit.hashCode()
        result = 31 * result + direccion.hashCode()
        result = 31 * result + pedidos.contentHashCode()
        result = 31 * result + platos.contentHashCode()
        result = 31 * result + horariosReserva.contentHashCode()
        result = 31 * result + puestosReserva
        result = 31 * result + disponibleTTG.hashCode()
        result = 31 * result + rating.contentHashCode()
        result = 31 * result + coord.hashCode()
        result = 31 * result + foto.hashCode()
        return result
    }

    override fun describeContents(): Int {
        return 0
    }

    @RequiresApi(Build.VERSION_CODES.Q)
    override fun writeToParcel(p0: Parcel?, p1: Int) {
        p0?.let {
            p0.writeString(id)
            p0.writeString(nombre)
            p0.writeString(correo)
            p0.writeString(telefono)
            p0.writeString(nit)
            p0.writeString(direccion)
            p0.writeTypedArray(pedidos,0)
            p0.writeTypedArray(platos,0)
            p0.writeStringArray(horariosReserva)
            p0.writeInt(puestosReserva)
            p0.writeBoolean(disponibleTTG)
            p0.writeIntArray(rating)
            p0.writeString(coord)
            p0.writeString(foto)
        }
    }
    companion object {
        @JvmField val CREATOR: Parcelable.Creator<Restaurante> = object : Parcelable.Creator<Restaurante> {
            override fun newArray(size: Int): Array<Restaurante?> = arrayOfNulls(size)

            @RequiresApi(Build.VERSION_CODES.Q)
            override fun createFromParcel(source: Parcel): Restaurante = Restaurante(source)

        }

    }

    @RequiresApi(Build.VERSION_CODES.Q)
    constructor(source: Parcel) : this(source.readString().toString(),source.readString().toString(),
        source.readString().toString(), source.readString().toString(),source.readString().toString(),
        source.readString().toString(), source.createTypedArray(Reserva.CREATOR) as Array<Reserva>,
        source.createTypedArray(Plato.CREATOR) as Array<Plato>,
        source.createStringArray() as Array<String>,source.readInt(),source.readBoolean(),source.createIntArray()
        ,source.readString().toString(),source.readString().toString())




    fun getRating(): String {
        val df = DecimalFormat("#.#")
        df.roundingMode = RoundingMode.FLOOR
        val size = if (rating!!.size.toDouble()<1) 1 else rating.size.toDouble()
        val suma = rating.reduce { cum, acc -> cum + acc }
        return df.format(suma.toDouble()/size.toDouble())
    }


    fun calculateDistance(userCoord: String): Double {
        val coordenadasUsuario : List<Double> = if(userCoord == "NaN" || userCoord.isBlank()){
            "0;0".split(";").map { it.toDouble() }
        }else{
            userCoord.split(";").map { it.toDouble() }
        }
        val result : FloatArray = floatArrayOf(0.0F)
        val coordenasRestaurante = coord.split(";").map { it.toDouble() }
        distanceBetween(coordenadasUsuario[0],coordenadasUsuario[1],coordenasRestaurante[0],coordenasRestaurante[1],result)
        return (result[0]/1000).toDouble()
    }


}