package com.example.restaurantorderapp.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.Gson


data class Cliente(

    var id:String,
    var nombre:String,
    var correo:String,
    var telefono:String,
    var pedidos: Array<Reserva>,
    var tipoDoc: String,
    var documento: String,
    var tarjetas: Array<Tarjeta>,
    var foto: String = "https://i.guim.co.uk/img/media/66767bbb27ae0e99d0dfb2975ff2a2b3db9e1c93/37_6_1612_967/master/1612.jpg?width=620&quality=85&auto=format&fit=max&s=67e825e39de42680ab64ca9a0de532f2"
): Parcelable {
    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(p0: Parcel?, p1: Int) {
        p0?.let {
            p0.writeString(id)
            p0.writeString(nombre)
            p0.writeString(correo)
            p0.writeString(telefono)
            p0.writeTypedArray(pedidos,0)
            p0.writeString(tipoDoc)
            p0.writeString(documento)
            p0.writeTypedArray(tarjetas,0)
            p0.writeString(foto)
        }
    }

    companion object{
        fun createFromJSON(jsonString:String) : Cliente{
            val gson =  Gson()
            return gson.fromJson(jsonString,Cliente::class.java)
        }
        @JvmField
        val CREATOR: Parcelable.Creator<Cliente> = object : Parcelable.Creator<Cliente> {
            override fun newArray(size: Int): Array<Cliente?> = arrayOfNulls(size)

            override fun createFromParcel(source: Parcel): Cliente = Cliente(source)

        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Cliente

        if (nombre != other.nombre) return false
        if (correo != other.correo) return false
        if (telefono != other.telefono) return false
        if (!pedidos.contentEquals(other.pedidos)) return false
        if (tipoDoc != other.tipoDoc) return false
        if (documento != other.documento) return false
        if (!tarjetas.contentEquals(other.tarjetas)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = nombre.hashCode()
        result = 31 * result + correo.hashCode()
        result = 31 * result + telefono.hashCode()
        result = 31 * result + pedidos.contentHashCode()
        result = 31 * result + tipoDoc.hashCode()
        result = 31 * result + documento.hashCode()
        result = 31 * result + tarjetas.contentHashCode()
        return result
    }

    override fun toString(): String {
        val gson = Gson()
        return gson.toJson(this).toString()
    }

    constructor(source: Parcel) : this(source.readString().toString(),source.readString().toString(),source.readString().toString(),
    source.readString().toString(),source.createTypedArray(Reserva.CREATOR) as Array<Reserva>,source.readString().toString(),
    source.readString().toString(),source.createTypedArray(Tarjeta.CREATOR) as Array<Tarjeta>,source.readString().toString())
}
