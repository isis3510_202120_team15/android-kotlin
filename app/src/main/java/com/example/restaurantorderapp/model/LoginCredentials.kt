package com.example.restaurantorderapp.model

import com.google.gson.Gson

data class LoginCredentials(
    var email: String,
    var pass: String,
){

    override fun toString(): String {
        val gson = Gson()
        return gson.toJson(this).toString()
    }

    companion object{
        fun createFromJSON(JSONString:String):LoginCredentials{
            val gson =  Gson()
            return gson.fromJson(JSONString,LoginCredentials::class.java)
        }
    }
}