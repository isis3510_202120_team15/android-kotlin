package com.example.restaurantorderapp.model

import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*


object DateHelper {
    fun dateGenerator():String {
        val ISO = "yyyy-MM-dd'T'HH:mm:ss'Z'"
        val tz: TimeZone = TimeZone.getTimeZone("UTC")
        val df: DateFormat =
            SimpleDateFormat(ISO) // Quoted "Z" to indicate UTC, no timezone offset

        df.timeZone = tz
        return df.format(Date())
    }
}