package com.example.restaurantorderapp.model

data class Resource<T>(
    val type: String,
    val payload: T?,
    val mesage:String?)