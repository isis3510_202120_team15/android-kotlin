package com.example.restaurantorderapp.model

import android.os.Parcel
import android.os.Parcelable

data class Tarjeta(
    val id: String?,
    val cardNumber: String,
    val cardHolder: String,
    val fecha: String
):Parcelable {
    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(p0: Parcel?, p1: Int) {
        p0?.let {
            p0.writeString(id)
            p0.writeString(cardNumber)
            p0.writeString(cardHolder)
            p0.writeString(fecha)
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Tarjeta

        if (cardNumber != other.cardNumber) return false
        if (cardHolder != other.cardHolder) return false
        if (fecha != other.fecha) return false

        return true
    }

    override fun hashCode(): Int {
        var result = cardNumber.hashCode()
        result = 31 * result + cardHolder.hashCode()
        result = 31 * result + fecha.hashCode()
        return result
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<Tarjeta> = object : Parcelable.Creator<Tarjeta> {
            override fun newArray(size: Int): Array<Tarjeta?> = arrayOfNulls(size)

            override fun createFromParcel(source: Parcel): Tarjeta = Tarjeta(source)

        }

    }
    constructor(source: Parcel) : this(source.readString().toString(),source.readString().toString(),source.readString().toString(),
    source.readString().toString())
}