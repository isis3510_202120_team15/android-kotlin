package com.example.restaurantorderapp.model

import com.google.gson.Gson

data class TokenId(
    val tokenId: String,
) {
    override fun toString(): String {
        val gson = Gson()
        return gson.toJson(this).toString()
    }
}