package com.example.restaurantorderapp.model

import com.google.gson.Gson

data class Measure(
    val feature:String,
    val horaInicio:String,
    val horaFinal:String?
){
    override fun toString(): String {
        val gson = Gson()
        return gson.toJson(this).toString()
    }
}
