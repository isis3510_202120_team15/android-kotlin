package com.example.restaurantorderapp.model

import android.os.Parcel
import android.os.Parcelable

data class TakeToGoOrder(
    var id:String,
    var clienteId: String,
    var restauranteId: String,
    var platos: Array<String>,
    var diaPedido: String,
    var horaPedido: String,
):Parcelable {
    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(p0: Parcel?, p1: Int) {
        p0?.let {
            p0.writeString(id)
            p0.writeString(clienteId)
            p0.writeString(restauranteId)
            p0.writeString(diaPedido)
            p0.writeString(horaPedido)
            p0.writeStringArray(platos)
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as TakeToGoOrder

        if (id != other.id) return false
        if (clienteId != other.clienteId) return false
        if (restauranteId != other.restauranteId) return false
        if (diaPedido != other.diaPedido) return false
        if (horaPedido != other.horaPedido) return false
        if (!platos.contentEquals(other.platos)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + clienteId.hashCode()
        result = 31 * result + restauranteId.hashCode()
        result = 31 * result + diaPedido.hashCode()
        result = 31 * result + horaPedido.hashCode()
        result = 31 * result + platos.contentHashCode()
        return result
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<Reserva> = object : Parcelable.Creator<Reserva> {
            override fun newArray(size: Int): Array<Reserva?> = arrayOfNulls(size)

            override fun createFromParcel(source: Parcel): Reserva = Reserva(source)

        }

    }
    constructor(source: Parcel) : this(source.readString().toString(), source.readString().toString(),
    source.readString().toString(), source.createStringArray() as Array<String>, source.readString().toString(),
    source.readString().toString())
}
