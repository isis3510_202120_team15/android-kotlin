package com.example.restaurantorderapp.activity.login

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.restaurantorderapp.api.Repository
import com.example.restaurantorderapp.model.Cliente
import com.example.restaurantorderapp.model.Measure
import com.example.restaurantorderapp.model.Message
import com.example.restaurantorderapp.model.Resource
import kotlinx.coroutines.launch


class LoginViewModel(private val repository: Repository) : ViewModel() {

    var client: MutableLiveData<Resource<Cliente>> = MutableLiveData()
    var tokenId: MutableLiveData<String> = MutableLiveData()

    fun getClientInfo(email: String) {
        viewModelScope.launch {
            val response = repository.getClienteByEmail(email)
            if (response.isSuccessful) {
                client.postValue(Resource("Cliente", response.body(), null))
            } else {
                client.postValue(Resource("Error", null, "Revisa tus credenciales"))
            }
        }
    }


    fun initiateMeasure(measure: Measure, connected: Boolean) {
        viewModelScope.launch {
            if (connected) {
                val response = repository.initiateMeasure(measure, connected)
                if(response?.isSuccessful==true){
                    tokenId.value = response.body()!!.tokenId
                }
            }
        }
    }


    fun endMeasure(tokenId:String,endHour: String, connected: Boolean) {
        if (connected) {
            viewModelScope.launch {
                repository.endMeasure(tokenId, Message(endHour), connected)
            }
        }
    }
}