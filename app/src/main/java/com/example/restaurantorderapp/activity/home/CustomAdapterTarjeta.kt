package com.example.restaurantorderapp.activity.home

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.restaurantorderapp.R
import com.example.restaurantorderapp.model.Tarjeta
import kotlinx.android.synthetic.main.creditcard_card.view.*

class CustomAdapterTarjeta(
    private val modelList: List<Tarjeta>,
    private val preferedCreditCard: String,
    var current: View? = null
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ViewHolder).bind(modelList[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        val layoutInflater = LayoutInflater.from(parent.context)

        return ViewHolder(layoutInflater.inflate(R.layout.creditcard_card, parent, false))
    }

    override fun getItemCount(): Int {
        return modelList.size
    }

    lateinit var mClickListener: ClickListener

    fun setOnItemClickListener(aClickListener: ClickListener) {
        mClickListener = aClickListener
    }

    interface ClickListener {
        fun onClick(pos: Int, aView: View)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView),
        View.OnClickListener {

        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(p0: View?) {
            current?.isSelected=false
            current = p0
            mClickListener.onClick(bindingAdapterPosition, itemView)
            p0?.isSelected = true
            notifyItemChanged(oldPosition)
        }

        fun bind(model: Tarjeta) {
            if(model.id==preferedCreditCard){
                current=itemView
                itemView.isSelected=true
            }

            itemView.txt_number.text = model.cardNumber

            itemView.txt_holder.text = model.cardHolder

            itemView.txt_fecha.text = model.fecha
        }
    }
}