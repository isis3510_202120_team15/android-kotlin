package com.example.restaurantorderapp.activity.signup

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.example.restaurantorderapp.R
import com.example.restaurantorderapp.activity.home.HomeActivity
import com.example.restaurantorderapp.activity.home.NetworkStatus
import com.example.restaurantorderapp.activity.home.NetworkStatusHelper
import com.example.restaurantorderapp.activity.login.LoginActivity
import com.example.restaurantorderapp.api.Repository
import com.example.restaurantorderapp.databinding.SingupLayoutBinding
import com.example.restaurantorderapp.model.*
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch


class SignupActivity : AppCompatActivity() {

    private val USER_LOGIN = "user_login"
    private val USER_DATA = "userdata"

    private lateinit var sharedPref: SharedPreferences

    private lateinit var viewModel: SignUpViewModel

    private var connected = true
    private lateinit var binding: SingupLayoutBinding
    private fun sendToHome(user_Info: String) {
        Toast.makeText(this@SignupActivity, "Bienvenido a Book Me Up", Toast.LENGTH_SHORT)
            .show()
        val intent = Intent(this@SignupActivity, HomeActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        intent.putExtra("user", user_Info)
        startActivity(intent)
        finish()
    }

    private fun markButtonDisable(button: Button) {
        button.isEnabled = false

    }

    private fun markButtonAvailable(button: Button) {
        button.isEnabled = true

    }
    private var alert: AlertDialog? = null
    private var snackbar:Snackbar? = null
    private fun makeAvailable(loginButton: Button){
        alert?.cancel()
        snackbar?.dismiss()
        markButtonAvailable(loginButton)
        snackbar = Snackbar.make(loginButton,"Conexion a internet establecida", Snackbar.LENGTH_SHORT)
        snackbar?.show()
    }

    private fun makeNetworkAlert(loginButton: Button) {
        snackbar?.dismiss()
        snackbar = Snackbar.make(loginButton,"Sin conexion", Snackbar.LENGTH_INDEFINITE)
        snackbar?.show()
        markButtonDisable(loginButton)
        val dialogBuilder = AlertDialog.Builder(this)
        // set message of alert dialog
        dialogBuilder.setMessage("Aun así, puede seguir usando algunas funcionalidades. Para volver " +
                "a tener acceso a todas las funcionalidades por favor revise su conexión a internet")
            // if the dialog is cancelable
            .setCancelable(true)
            // positive button text and action
            .setPositiveButton("Ok") { _, _ -> }

        // create dialog box
        alert = dialogBuilder.create()
        // set title for alert dialog box
        alert?.setTitle("No hay conexión a internet")
        alert?.setIcon(R.mipmap.ic_launcher)
        // show alert dialog
        alert?.show()
    }

    override fun onCreate(savedInstanceState: Bundle?) {

        sharedPref = this.getPreferences(Context.MODE_PRIVATE)
        val repository = Repository()
        val viewModelFactory = SignUpViewModelFactory(repository)
        viewModel = ViewModelProvider(this, viewModelFactory)[SignUpViewModel::class.java]

        super.onCreate(savedInstanceState)
        binding = SingupLayoutBinding.inflate(layoutInflater)
        setContentView(binding.root)
        // Botones
        val registerButton: Button = binding.registerButton
        val regresarTexto: TextView = binding.regresarTexto
        val regresarIcono: ImageView = binding.regresarIcono
        ///--------------- Check Network Connection ------------------------------

        NetworkStatusHelper(this@SignupActivity).observe(this, {
            connected = when (it) {
                NetworkStatus.Available -> {
                    makeAvailable(registerButton);true
                }
                NetworkStatus.Unavailable -> {
                    makeNetworkAlert(registerButton);false
                }
            }
        })

        //Inputs
        val nombre: EditText = binding.nombreInput
        val email: EditText = binding.registroCorreoInput
        val pswd: EditText = binding.registroContraseniaInput
        val telefono: EditText = binding.telefonoInput
        val documento: EditText = binding.documentoInput
        val tipoDocGrupo: RadioGroup = binding.tipoDocOpciones
        var tipoDoc: RadioButton

        registerButton.setOnClickListener {

            regresarIcono.setOnClickListener {
                val loginIntent = Intent(this@SignupActivity, LoginActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(loginIntent)
                finish()
            }

            regresarTexto.setOnClickListener {
                val loginIntent = Intent(this@SignupActivity, LoginActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(loginIntent)
                finish()
            }

            val selectedOption: Int = tipoDocGrupo.checkedRadioButtonId
            var inputTipo = ""

            try {
                tipoDoc = findViewById(selectedOption)
                inputTipo = tipoDoc.text.toString()
            } catch (e: Exception) {
                Toast.makeText(
                    this@SignupActivity,
                    "Selecciona un tipo de documento",
                    Toast.LENGTH_SHORT
                ).show()
            }

            val inputNombre = nombre.text.toString()
            val inputEmail = email.text.toString()
            val inputPswd = pswd.text.toString()
            val inputTelefono = telefono.text.toString()
            val inputDoc = documento.text.toString()

            //Validar que las entradas no sean vacias
            when {
                TextUtils.isEmpty(inputNombre) -> {
                    Toast.makeText(
                        this@SignupActivity,
                        "Por favor ingrese su nombre",
                        Toast.LENGTH_SHORT
                    ).show()
                }
                TextUtils.isEmpty(inputEmail) -> {
                    Toast.makeText(
                        this@SignupActivity,
                        "Por favor ingrese su correo",
                        Toast.LENGTH_SHORT
                    ).show()
                }
                TextUtils.isEmpty(inputPswd) -> {
                    Toast.makeText(
                        this@SignupActivity,
                        "Por favor ingrese la contraseña que desea",
                        Toast.LENGTH_SHORT
                    ).show()
                }
                TextUtils.isEmpty(inputTelefono) -> {
                    Toast.makeText(
                        this@SignupActivity,
                        "Por favor ingrese su telefono",
                        Toast.LENGTH_SHORT
                    ).show()
                }
                TextUtils.isEmpty(inputDoc) -> {
                    Toast.makeText(
                        this@SignupActivity,
                        "Por favor ingrese su documento",
                        Toast.LENGTH_SHORT
                    ).show()
                }
                TextUtils.isEmpty(inputTipo) -> {
                    Toast.makeText(
                        this@SignupActivity,
                        "Por favor ingrese el tipo de documento",
                        Toast.LENGTH_SHORT
                    ).show()
                }
                else -> {

                    val newCliente = Cliente(
                        "x", inputNombre, inputEmail, inputTelefono,
                        emptyArray(), inputTipo, inputDoc,
                        emptyArray()
                    )



                    lifecycleScope.launch(Dispatchers.Main) {

                        // Firebase user creation
                        val firebaseJob = async(Dispatchers.IO) {
                            try {
                                val auth = FirebaseAuth.getInstance()
                                    .createUserWithEmailAndPassword(inputEmail, inputPswd)
                                while (!auth.isComplete) {
                                    /** Do nothing while Auth is completed} */
                                }
                                if (auth.isSuccessful) return@async Resource("Boolean", true, null)
                                else return@async Resource("Boolean", false, null)
                            }catch (error:Exception){
                                Log.e("ERRROR AQUI","sdsdfsdkjfhsdakjfhskdajfhskdjfhskdjfhksdafhkdslj")
                                return@async Resource("Error",null,error.message)
                            }
                        }

                        //Backend Creation
                        val backendJob = async(Dispatchers.IO) {
                            viewModel.createCliente(newCliente)
                            while (viewModel.resource.value==null) {
                                /** Do nothing while fetch is completed*/
                            }
                            return@async viewModel.resource
                        }

                        val creation = firebaseJob.await()
                        val resp = backendJob.await().value
                        if (creation.type=="Boolean" && resp!!.type == "Cliente") {
                            with(sharedPref.edit()) {
                                val credentials = LoginCredentials(inputEmail,inputPswd)
                                putString(USER_LOGIN, credentials.toString())
                                putString(USER_DATA,resp.payload.toString())
                                apply()
                            }
                            sendToHome(resp.payload.toString())
                        } else if (creation.type=="Error" && resp!!.type == "Cliente") {
                            viewModel.deleteCliente(resp.payload!!.id)
                            Toast.makeText(applicationContext,creation.mesage,Toast.LENGTH_SHORT).show()
                        } else if (creation.type=="Bolean" && resp!!.type == "Error") {
                            FirebaseAuth.getInstance().currentUser!!.delete()
                            Toast.makeText(applicationContext, resp.mesage, Toast.LENGTH_SHORT).show()
                        }else{
                            Log.e("ERRROR","sdlkfj;sldkfjsl;dkjf;saldkfj;l")
                        }
                    }
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()

        if(!isNetworkConnected())
        {
            val registerButton: Button = binding.registerButton
            makeNetworkAlert(registerButton)
        }
    }

    override fun onStart() {
        super.onStart()
        lifecycleScope.launch(Dispatchers.IO) {
            val measure = Measure("Registro", DateHelper.dateGenerator(), "")
            viewModel.initiateMeasure(measure, connected = connected)
        }
    }

    override fun onStop() {
        viewModel.endMeasure(viewModel.tokenId.value!!,DateHelper.dateGenerator(), connected = connected)
        super.onStop()
        
    }


    private fun isNetworkConnected(): Boolean {
        //1
        val connectivityManager =
            getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        //2
        val activeNetwork = connectivityManager.activeNetworkInfo
        //4
        return activeNetwork != null
    }
}