package com.example.restaurantorderapp.activity.home

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.restaurantorderapp.R
import com.example.restaurantorderapp.model.TakeToGoOrder
import kotlinx.android.synthetic.main.taketogo_card.view.*

class CustomAdapterPedidos(private val modelList: MutableList<TakeToGoOrder>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ViewHolder).bind(modelList[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(layoutInflater.inflate(R.layout.taketogo_card, parent, false))
    }

    override fun getItemCount(): Int {
        return modelList.size
    }

    lateinit var mClickListener: ClickListener

    fun setOnItemClickListener(aClickListener: ClickListener) {
        mClickListener = aClickListener
    }

    interface ClickListener {
        fun onClick(pos: Int, aView: View)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(p0: View?) {
            mClickListener.onClick(adapterPosition, itemView)
        }

        fun bind(model: TakeToGoOrder) {
            itemView.txt_nameP.text = model.id

            itemView.txt_diaP.text = "Dia Pedido: "+model.diaPedido
            itemView.txt_horaP.text = "Hora Pedido: "+model.horaPedido

        }

    }

}
