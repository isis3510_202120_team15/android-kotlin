package com.example.restaurantorderapp.activity.home

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.restaurantorderapp.R
import com.example.restaurantorderapp.model.Reserva
import kotlinx.android.synthetic.main.reserva_card.view.*
import kotlinx.android.synthetic.main.restaurant_card.view.txt_nameR

class CustomAdapterReserva(private val modelList: MutableList<Reserva>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ViewHolder).bind(modelList[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(layoutInflater.inflate(R.layout.reserva_card, parent, false))
    }

    override fun getItemCount(): Int {
        return modelList.size
    }

    lateinit var mClickListener: ClickListener

    fun setOnItemClickListener(aClickListener: ClickListener) {
        mClickListener = aClickListener
    }

    interface ClickListener {
        fun onClick(pos: Int, aView: View)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(p0: View?) {
            mClickListener.onClick(adapterPosition, itemView)
        }

        fun bind(model: Reserva) {
            itemView.txt_nameR.text = model.id

            itemView.txt_puestosR.text = "Puestos: "+model.puestos
            itemView.txt_diaR.text = "Dia Reserva: "+model.diaReserva
            itemView.txt_horaR.text = "Hora Reserva: "+model.horaReserva

        }

    }


}