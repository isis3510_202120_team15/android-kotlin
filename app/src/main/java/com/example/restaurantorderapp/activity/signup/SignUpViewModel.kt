package com.example.restaurantorderapp.activity.signup

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.restaurantorderapp.api.Repository
import com.example.restaurantorderapp.model.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class SignUpViewModel(private val repository: Repository) : ViewModel() {

    var resource: MutableLiveData<Resource<Cliente>> = MutableLiveData()
    var tokenId: MutableLiveData<String> = MutableLiveData()

    fun createCliente(newCliente: Cliente) {
        viewModelScope.launch {
            val response = repository.createCliente(newCliente)
            if (response.isSuccessful) {
                resource.postValue(Resource("Cliente",response.body(),null))
            } else {
                val error = response.errorBody()!!.string()
                val stringError = ErrorHandler(error).toString()
                resource.postValue(Resource("Error",null,stringError))
            }
        }
    }

    fun deleteCliente(id: String) {
        viewModelScope.launch(Dispatchers.Unconfined) {
            repository.deleteCliente(id)
        }
    }

    fun initiateMeasure(measure: Measure, connected: Boolean) {
        viewModelScope.launch {
            if (connected) {
                val response = repository.initiateMeasure(measure,connected)
                if(response?.isSuccessful==null){
                    tokenId.setValue(response?.body()!!.tokenId)}
                else{
                    Log.e("WATCHDOG","ERROR SENDIND TOKEN")}
            }
        }
    }


    fun endMeasure(tokenId:String,endHour: String, connected: Boolean) {
        if (connected) {
            viewModelScope.launch {
                repository.endMeasure(tokenId, Message(endHour),connected)
            }
        }
    }
}