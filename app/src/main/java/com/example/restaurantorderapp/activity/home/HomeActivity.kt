package com.example.restaurantorderapp.activity.home

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.net.ConnectivityManager
import android.os.Bundle
import android.os.Looper
import android.provider.Settings
import android.view.Menu
import android.view.MenuItem
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.example.restaurantorderapp.R
import com.example.restaurantorderapp.activity.login.LoginActivity
import com.example.restaurantorderapp.api.Repository
import com.example.restaurantorderapp.databinding.ActivityMainBinding
import com.example.restaurantorderapp.model.Cliente
import com.google.android.gms.location.*
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.coroutines.*
import java.util.*


class HomeActivity : AppCompatActivity() {
    private var coordenadas: MutableLiveData<String> = MutableLiveData("")

    private val USER_DATA = "userdata"
    private val USER_COORD = "usercoord"

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var binding: ActivityMainBinding
    private lateinit var sharedPref: SharedPreferences

    //drawer
    private lateinit var drawerLayout: DrawerLayout

    var conectado: MutableLiveData<Boolean> = MutableLiveData(true)

    var repository: Repository = Repository()
    lateinit var cliente: Cliente


    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        sharedPref = this.getPreferences(Context.MODE_PRIVATE)
        binding = ActivityMainBinding.inflate(layoutInflater)


        lifecycleScope.launch {
            NetworkStatusHelper(applicationContext).observe(this@HomeActivity, {
                when (it) {
                    NetworkStatus.Available -> {
                        conectado.postValue(true)
                    }
                    NetworkStatus.Unavailable -> {
                        conectado.postValue(false)
                    }
                }
            })
        }


        //if(hay conexion){
        setContentView(R.layout.fragment_home)
        /**} else{
         *setContentView(R.layout.noConexionFragement)
        }*/
        setContentView(binding.root)
        ///--------------- Check Network Connection ------------------------------

        lifecycleScope.launch(Dispatchers.Unconfined) {
            //Using Bandle
            val data = intent.extras!!.getString("user")
            cliente = Cliente.createFromJSON(data!!)

            val email = cliente.correo
            val name = cliente.nombre
            createDrawer(name, email)
            with(sharedPref.edit()) {
                putString(USER_DATA, cliente.toString())
                apply()
            }
        }

        lifecycleScope.launch(Dispatchers.Main) {
            requestNewLocationData()
            var coord =
                withContext(Dispatchers.IO) {
                    sharedPref.getString(
                        USER_COORD,
                        "NaN"
                    )
                }!!
            if (coord == "") {
                coord = getLastLocation()
            }
            coordenadas.postValue(coord)
        }
    }



    override fun onResume() {
        super.onResume()
        if (!isNetworkConnected()) {
            makeNetworkAlert()
        }
    }

    private var alert: AlertDialog? = null
    private fun makeNetworkAlert() {
        val dialogBuilder = AlertDialog.Builder(this)
        // set message of alert dialog
        dialogBuilder.setMessage("Aun así, puede seguir usando algunas funcionalidades. Para volver " +
                "a tener acceso a todas las funcionalidades por favor revise su conexión a internet")
            // if the dialog is cancelable
            .setCancelable(true)
            // positive button text and action
            .setPositiveButton("Ok") { _, _ ->

            }


        // create dialog box
        alert = dialogBuilder.create()
        // set title for alert dialog box
        alert?.setTitle("No hay conexión a internet")
        alert?.setIcon(R.mipmap.ic_launcher)
        // show alert dialog
        alert?.show()
    }

    private fun createDrawer(name: String, email: String) {
        setSupportActionBar(binding.appBarMain.toolbar)
        drawerLayout = binding.drawerLayout
        val navView: NavigationView = binding.navView
        //Set user and email
        val header = navView.getHeaderView(0)
        header.findViewById<TextView>(R.id.userName).text = name
        header.findViewById<TextView>(R.id.userEmail).text = email
        val navController = findNavController(R.id.nav_host_fragment_content_main)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_home, R.id.nav_payment_methods, R.id.nav_my_profile,
                R.id.nav_help, R.id.nav_reservation_history, R.id.listarTakeToGo,
                R.id.succesFragment, R.id.succesP
            ), drawerLayout
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.signout -> {
                with(sharedPref.edit()) {
                    putString(USER_DATA, "{}")
                    apply()
                }
                Toast.makeText(this@HomeActivity,
                    "You are login out",
                    Toast.LENGTH_SHORT)
                    .show()
                val intent = Intent(this@HomeActivity, LoginActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                intent.putExtra("logout", "logout")
                startActivity(intent)
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment_content_main)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }


    /////----------------------------------LOCATION---------------------------------------

    private val PERMISSION_ID = 42
    private lateinit var mFusedLocationClient: FusedLocationProviderClient


    private fun getLastLocation(): String {
        var coordResult = ""
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            if (isLocationEnabled()) {
                mFusedLocationClient.lastLocation.addOnCompleteListener(this) { task ->
                    val location: Location? = task.result
                    if (location == null) {
                        requestNewLocationData()
                    } else {
                        coordResult =
                            location.latitude.toString() + ";" + location.longitude.toString()

                    }
                }
            } else {
                Toast.makeText(
                    this,
                    "Verifica los permisos de localizaciion",
                    Toast.LENGTH_LONG
                ).show()
                val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                startActivity(intent)
            }
        } else {
            requestPermissions()
        }
        with(this@HomeActivity.sharedPref.edit()) {
            putString(this@HomeActivity.USER_COORD, coordResult)
            apply()
        }
        return coordResult
    }

    fun requestNewLocationData() {
        val mLocationRequest = LocationRequest.create().apply {
            interval = 100
            fastestInterval = 50
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
            maxWaitTime = 100
        }

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED ||
            ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            requestPermissions()
        }
        mFusedLocationClient.requestLocationUpdates(
            mLocationRequest, mLocationCallback, Looper.getMainLooper()
        )
    }

    private val mLocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            val mLastLocation: Location = locationResult.lastLocation
            val coordResult =
                mLastLocation.latitude.toString() + ";" + mLastLocation.longitude.toString()
            coordenadas.postValue(coordResult)
            with(this@HomeActivity.sharedPref.edit()) {
                putString(this@HomeActivity.USER_COORD, coordResult)
                apply()
            }
            }
    }

    private fun isLocationEnabled(): Boolean {
        val locationManager: LocationManager =
            getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
            LocationManager.NETWORK_PROVIDER
        )
    }

    private fun requestPermissions() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION
            ),
            PERMISSION_ID
        )
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray,
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == PERMISSION_ID) {
            if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                getLastLocation()
            }
        }
    }

    private fun isNetworkConnected(): Boolean {
        //1
        val connectivityManager = getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        //2
        val activeNetwork = connectivityManager.activeNetworkInfo
        //4
        return activeNetwork != null
    }
}
