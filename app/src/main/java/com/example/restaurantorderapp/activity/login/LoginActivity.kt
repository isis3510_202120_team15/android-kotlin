package com.example.restaurantorderapp.activity.login

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.os.Bundle
import android.text.TextUtils
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.example.restaurantorderapp.R
import com.example.restaurantorderapp.activity.home.HomeActivity
import com.example.restaurantorderapp.activity.home.NetworkStatus
import com.example.restaurantorderapp.activity.home.NetworkStatusHelper
import com.example.restaurantorderapp.activity.signup.SignupActivity
import com.example.restaurantorderapp.api.Repository
import com.example.restaurantorderapp.databinding.LoginLayoutBinding
import com.example.restaurantorderapp.model.DateHelper
import com.example.restaurantorderapp.model.LoginCredentials
import com.example.restaurantorderapp.model.Measure
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.fragment_detail.*
import kotlinx.coroutines.*
import java.util.*
import kotlin.NoSuchElementException


class LoginActivity : AppCompatActivity() {


    private lateinit var viewModel: LoginViewModel

    private lateinit var sharedPref: SharedPreferences

    private val USER_LOGIN = "user_login"


    private var connected = true
    private lateinit var binding: LoginLayoutBinding

    private fun sendToHome(user_Info: String) {
        Toast.makeText(this@LoginActivity, "You are logged in succesfully", Toast.LENGTH_SHORT)
            .show()
        val intent = Intent(this@LoginActivity, HomeActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        intent.putExtra("user", user_Info)
        startActivity(intent)
        finish()
    }

    private fun sendToRegister() {
        val signUpIntent = Intent(this@LoginActivity, SignupActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(signUpIntent)
        finish()
    }

    private fun markButtonDisable(button: Button) {
        button.isEnabled = false
        //button.setTextColor(ContextCompat.getColor(context, R.color.common_google_signin_btn_text_light_disabled))
        //button.setBackgroundColor(ContextCompat.getColor(textView.context, R.color.material_on_background_disabled))
    }

    private fun markButtonAvailable(button: Button) {
        button.isEnabled = true
        //button.setTextColor(ContextCompat.getColor(textView.context, R.color.Yellow))
        //button.setBackgroundColor(ContextCompat.getColor(textView.context, R.color.Light_Text_Color))
    }

    private var alert: AlertDialog? = null
    private var snackbar: Snackbar? = null
    private fun makeAvailable(loginButton: Button) {
        alert?.cancel()
        markButtonAvailable(loginButton)
        snackbar?.dismiss()
        snackbar =
            Snackbar.make(loginButton, "Conexion a internet establecida", Snackbar.LENGTH_SHORT)
        snackbar?.show()
    }

    private fun makeNetworkAlert(loginButton: Button) {
        snackbar?.dismiss()
        snackbar = Snackbar.make(loginButton, "Sin conexion", Snackbar.LENGTH_INDEFINITE)
        snackbar?.show()
        markButtonDisable(loginButton)
        val dialogBuilder = AlertDialog.Builder(this)
        // set message of alert dialog
        dialogBuilder.setMessage("Aun así, puede seguir usando algunas funcionalidades. Para volver " +
                "a tener acceso a todas las funcionalidades por favor revise su conexión a internet")
            // if the dialog is cancelable
            .setCancelable(true)
            // positive button text and action
            .setPositiveButton("Ok") { _, _ ->
            }

        // create dialog box
        alert = dialogBuilder.create()
        // set title for alert dialog box
        alert?.setTitle("No hay conexión a internet")
        alert?.setIcon(R.mipmap.ic_launcher)
        // show alert dialog
        alert?.show()

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = LoginLayoutBinding.inflate(layoutInflater)
        setContentView(binding.root)
        sharedPref = this.getPreferences(Context.MODE_PRIVATE)

        val exceptionHandler = CoroutineExceptionHandler { _, throwable ->
            Toast.makeText(applicationContext, throwable.message, Toast.LENGTH_SHORT).show()
        }
        val repository = Repository()
        val viewModelFactory = LoginViewModelFactory(repository)
        viewModel = ViewModelProvider(this, viewModelFactory)[LoginViewModel::class.java]

        val loginButton: Button = binding.loginButton


        ///------------- Login Activity -----------------------------------------

        val email: EditText = binding.inputCorreo
        val pswd: EditText = binding.inputContrasenia
        val register: TextView = binding.registrate


        /**
         * Register Link
         */
        register.setOnClickListener {
            sendToRegister()
        }

        /**
         * Login Buton
         */
        loginButton.setOnClickListener {

            //Get Inputs
            val inputEmail: String = email.text.toString().trim { it <= ' ' }
            val inputPswd: String = pswd.text.toString().trim { it <= ' ' }

            //Check inputs are not empty
            when {
                TextUtils.isEmpty(inputEmail) -> {
                    Toast.makeText(
                        this@LoginActivity,
                        "Por favor ingresa tu correo",
                        Toast.LENGTH_SHORT
                    ).show()
                }
                TextUtils.isEmpty(inputPswd) -> {
                    Toast.makeText(
                        this@LoginActivity,
                        "Por favor ingresa tu contraseña",
                        Toast.LENGTH_SHORT
                    ).show()
                }

                else -> {
                    //Start Main Coroutine for checking firebase and backEnd
                    lifecycleScope.launch(Dispatchers.Main + exceptionHandler) {

                        // FirebaseAuth Job
                        val firebaseJob = async(Dispatchers.IO) {
                            val auth = FirebaseAuth.getInstance()
                                .signInWithEmailAndPassword(inputEmail, inputPswd)
                            while (!auth.isComplete) {
                                /** Do nothing while Auth is completed*/
                            }
                            return@async auth.isSuccessful

                        }

                        //Fetch User Data Job
                        val backEndJob =
                            async(Dispatchers.IO) {

                                viewModel.getClientInfo(inputEmail)
                                while (viewModel.client.value == null) {
                                    /** Do nothing while fetch is completed*/
                                }
                                return@async viewModel.client.value
                            }

                        // Resultado de los trabajos
                        val loginSuccessful = firebaseJob.await()
                        val cliente = backEndJob.await()


                        if (loginSuccessful && cliente!!.type == "Cliente") {
                            with(sharedPref.edit()) {
                                val credentials = LoginCredentials(inputEmail, inputPswd)
                                putString(USER_LOGIN, credentials.toString())
                                apply()
                            }
                            sendToHome(cliente.payload.toString())
                        } else {
                            Toast.makeText(
                                this@LoginActivity,
                                "Por favor revisa tus credenciales",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                }
            }
        }

        ///--------------- Check Network Connection ------------------------------

        NetworkStatusHelper(this@LoginActivity).observe(this, {
            connected = when (it) {
                NetworkStatus.Available -> {
                    makeAvailable(loginButton);true
                }
                NetworkStatus.Unavailable -> {
                    makeNetworkAlert(loginButton);false
                }
            }
        })
    }

    override fun onResume() {

        super.onResume()
        if (!isNetworkConnected()) {
            val loginButton: Button = binding.loginButton
            makeNetworkAlert(loginButton)
        }

        val data = intent.extras?.getString("logout")
        if (data != null) {
            FirebaseAuth.getInstance().signOut()
            with(sharedPref.edit()) {
                putString(USER_LOGIN, "{}")
                apply()
            }
        }
    }

    override fun onStart() {
        super.onStart()
        lifecycleScope.launch(Dispatchers.IO) {
            val measure = Measure("Login", DateHelper.dateGenerator(), "")
            viewModel.initiateMeasure(measure, connected = connected)
        }

        //Go to next section of the app
        lifecycleScope.launch(Dispatchers.Main) {
            val data = intent.extras?.getString("logout")
            if (isNetworkConnected() && data == null) {
                val jsonString = sharedPref.getString(USER_LOGIN, "{}")
                if (jsonString != "{}" && !jsonString.isNullOrBlank() && !jsonString.isNullOrEmpty()) {
                    val credentials = LoginCredentials.createFromJSON(jsonString)
                    val backEndJob =
                        async(Dispatchers.IO) {
                            try {
                                viewModel.getClientInfo(credentials.email)
                                while (viewModel.client.value == null) {
                                    /** Do nothing while fetch is completed*/
                                }
                            } catch (e: NoSuchElementException) {
                                Toast.makeText(
                                    this@LoginActivity,
                                    e.message,
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                            return@async viewModel.client
                        }
                    val cliente = backEndJob.await().value
                    sendToHome(cliente!!.payload.toString())
                }
            } else if (data != null) {
                FirebaseAuth.getInstance().signOut()
                with(sharedPref.edit()) {
                    putString(USER_LOGIN, "")
                    apply()
                }
            }
        }

    }


    private fun isNetworkConnected(): Boolean {
        //1
        val connectivityManager = getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        //2
        val activeNetwork = connectivityManager.activeNetworkInfo
        //4
        return activeNetwork != null
    }

    override fun onStop() {
        viewModel.endMeasure(viewModel.tokenId.value!!,DateHelper.dateGenerator(), connected = connected)
        super.onStop()
    }

}
