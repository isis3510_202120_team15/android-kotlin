package com.example.restaurantorderapp.activity.home

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.example.restaurantorderapp.R
import com.example.restaurantorderapp.model.Restaurante
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.restaurant_card.view.*

class CustomAdapter(private val modelList: List<Restaurante>, private val userCoords:String) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ViewHolder).bind(modelList[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        val layoutInflater = LayoutInflater.from(parent.context)

        return ViewHolder(layoutInflater.inflate(R.layout.restaurant_card, parent, false))
    }

    override fun getItemCount(): Int {
        return modelList.size
    }

    lateinit var mClickListener: ClickListener

    fun setOnItemClickListener(aClickListener: ClickListener) {
        mClickListener = aClickListener
    }

    interface ClickListener {
        fun onClick(pos: Int, aView: View)
    }

    inner  class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView),
        View.OnClickListener {

        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(p0: View?) {
            mClickListener.onClick(bindingAdapterPosition, itemView)
        }

        fun bind(model: Restaurante) {
            itemView.txt_nameR.text = model.nombre

            itemView.txt_rating.text = model.getRating()

            itemView.txt_distance.text =
                "${String.format("%.2f", model.calculateDistance(userCoords))} km"
            bindUrlImage(itemView.img,model.foto)



        }
        private fun bindUrlImage(view: ImageView, imageUrl: String?) {
            if (imageUrl != null) {
                Picasso.get()
                    .load(imageUrl)
                    .fit()
                    .centerCrop()
                    .into(view)
            } else {
                view.setImageBitmap(null)
            }
        }
    }

}