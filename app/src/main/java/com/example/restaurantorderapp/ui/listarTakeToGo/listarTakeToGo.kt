package com.example.restaurantorderapp.ui.listarTakeToGo

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.restaurantorderapp.activity.home.CustomAdapterPedidos
import com.example.restaurantorderapp.activity.home.HomeActivity
import com.example.restaurantorderapp.activity.home.NetworkStatus
import com.example.restaurantorderapp.activity.home.NetworkStatusHelper
import com.example.restaurantorderapp.databinding.ListarTakeToGoFragmentBinding
import com.example.restaurantorderapp.model.Cliente
import com.example.restaurantorderapp.model.DateHelper
import com.example.restaurantorderapp.model.Measure
import com.example.restaurantorderapp.model.TakeToGoOrder
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class listarTakeToGo : Fragment() {


    private lateinit var actividad: HomeActivity
    private lateinit var sharedRef: SharedPreferences
    private lateinit var ViewModel: ListarTakeToGoViewModel
    private var _binding: ListarTakeToGoFragmentBinding? = null
    private val gson = Gson()
    private val USER_DATA = "userdata"

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!
    private var conectado: Boolean? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        actividad = activity as HomeActivity
        val repository = actividad.repository
        sharedRef = requireActivity().getPreferences(Context.MODE_PRIVATE)
        val viewModelFactory = ListarTakeToGoViewModelFactory(repository)
        ViewModel =
            ViewModelProvider(this, viewModelFactory)[ListarTakeToGoViewModel::class.java]
        _binding = ListarTakeToGoFragmentBinding.inflate(inflater, container, false)
        val root: View = binding.root
        //Bandle to obtain id of current user
        val data = sharedRef.getString(USER_DATA, "NaN")
        val cliente = gson.fromJson(data, Cliente::class.java)
        //Recycler View
        var pedidoList: MutableList<TakeToGoOrder>

        lifecycleScope.launch {
            actividad.conectado.observe(viewLifecycleOwner, {
                val measure = Measure("Listar ordenes", DateHelper.dateGenerator(), "")
                if (it) {
                    ViewModel.getPedidos(cliente.id, true)
                    conectado = true
                    ViewModel.initiateMeasure(measure, connected = true)
                } else {
                    ViewModel.getPedidos(cliente.id, false)
                    conectado = false
                    ViewModel.initiateMeasure(measure, connected = false)
                }
            })
        }


        lifecycleScope.launch(Dispatchers.Main)
        {
            val reservaObserver = Observer<List<TakeToGoOrder>> { pedidos ->
                pedidoList = pedidos as MutableList<TakeToGoOrder>
                val rcv_reserva = binding.rcvTaketogo
                val adapter = CustomAdapterPedidos(pedidoList)
                rcv_reserva.layoutManager =
                    LinearLayoutManager(context, RecyclerView.VERTICAL, false)
                rcv_reserva.adapter = adapter
                adapter.setOnItemClickListener(object : CustomAdapterPedidos.ClickListener {
                    override fun onClick(pos: Int, aView: View) {
                        Toast.makeText(context, pedidoList[pos].id, Toast.LENGTH_SHORT).show()

                    }
                })
            }
            ViewModel.taketogo.observe(viewLifecycleOwner, reservaObserver)
        }
        ///--------------- Check Network Connection ------------------------------

        NetworkStatusHelper(requireContext()).observe(viewLifecycleOwner, {
            when (it) {
                NetworkStatus.Available -> makeAvailable(binding.rcvTaketogo)
                NetworkStatus.Unavailable -> makeNetworkAlert(binding.rcvTaketogo)
            }
        })

        return root
    }

    private var snackbar: Snackbar? = null
    private fun makeNetworkAlert(button: RecyclerView) {
        snackbar?.dismiss()
        snackbar = Snackbar.make(button, "Sin conexión", Snackbar.LENGTH_INDEFINITE)
        snackbar?.show()
    }

    private fun makeAvailable(button: RecyclerView) {
        if (snackbar != null) {
            snackbar?.dismiss()
            snackbar =
                Snackbar.make(button, "Conexión a internet establecida", Snackbar.LENGTH_SHORT)
            snackbar?.show()

        } else {
            snackbar =
                Snackbar.make(button, "Conexión a internet establecida", Snackbar.LENGTH_SHORT)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onStop() {
        ViewModel.endMeasure(ViewModel.tokenId.value,
            DateHelper.dateGenerator(),
            connected = conectado!!)
        super.onStop()
    }

}