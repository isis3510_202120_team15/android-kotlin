package com.example.restaurantorderapp.ui.pedido

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.text.Editable
import android.text.SpannableStringBuilder
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import com.example.restaurantorderapp.R
import com.example.restaurantorderapp.activity.home.HomeActivity
import com.example.restaurantorderapp.databinding.FragmentPedidoBinding
import com.example.restaurantorderapp.model.*
import com.example.restaurantorderapp.ui.detail.DatePickerFragment
import com.example.restaurantorderapp.ui.detail.MultiSelectionSpinner
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import kotlinx.coroutines.launch

class FragmentPedido : Fragment() {

    private lateinit var sharedRef: SharedPreferences
    private var _binding: FragmentPedidoBinding? = null
    private val binding get() = _binding!!
    private val gson = Gson()
    private val USER_DATA = "userdata"

    //Doing a order
    private lateinit var order: TakeToGoOrder

    //selected restaurant
    private lateinit var selectedR: Restaurante
    private lateinit var viewModel: FragmentPedidoViewModel
    private var conectado: Boolean? = null
    private lateinit var actividad: HomeActivity


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        actividad = activity as HomeActivity
        val repository = actividad.repository
        sharedRef = requireActivity().getPreferences(Context.MODE_PRIVATE)
        val ViewModelFactory = FragmentPedidoViewModelFactory(repository)
        viewModel = ViewModelProvider(this, ViewModelFactory)[FragmentPedidoViewModel::class.java]
        _binding = FragmentPedidoBinding.inflate(inflater, container, false)
        val root: View = binding.root

        //Look for current user
        val data = sharedRef.getString(USER_DATA, "NaN")
        val user = gson.fromJson(data, Cliente::class.java)

        //Look for current restaurant info
        selectedR = arguments?.getParcelable("selected")!!
        val name = selectedR.nombre
        val id: String = selectedR.id
        binding.nameRP.text = name
        //Creating a basic Order
        order = TakeToGoOrder("Aa", user.id, id, emptyArray(), "", "")
        //Show available hours of the restaurant
        val hour: Array<String> = selectedR.horariosReserva
        val spinner: Spinner = binding.hourSpinerP
        val array_adapter =
            ArrayAdapter(requireContext(), R.layout.support_simple_spinner_dropdown_item, hour)
        array_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner.adapter = array_adapter
        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                println("erreur")
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long,
            ) {
                val type = parent?.getItemAtPosition(position).toString()
                order.horaPedido = type
            }
        }

        //setting dates picker
        val fecha: EditText = binding.txtTimeP
        fecha.setOnClickListener {
            showDatePickerDialog(fecha)
        }
        val multispiner: MultiSelectionSpinner = binding.platSpinerP
        val platos: Array<Plato> = selectedR.platos
        multispiner.setItems(platos)
        //Action in button reservar
        val btn_order = binding.button
        btn_order.setOnClickListener {
            val platosS: Array<String> = multispiner.getSelectedItems()
            order.platos = platosS
            //entradas vacias
            when {
                order.platos.isEmpty() -> {
                    Toast.makeText(
                        requireContext(),
                        "Por favor ingrese los platos a pedir",
                        Toast.LENGTH_SHORT
                    ).show()
                }
                order.horaPedido === "" -> {
                    Toast.makeText(
                        requireContext(),
                        "Por favor ingrese la hora a reservar",
                        Toast.LENGTH_SHORT
                    ).show()
                }
                order.diaPedido === "" -> {
                    Toast.makeText(
                        requireContext(),
                        "Por favor ingrese el dia a reservar",
                        Toast.LENGTH_SHORT
                    ).show()
                }
                else -> {
                    Log.e("Order", order.toString())
                    viewModel.postOrder(order)
                    Toast.makeText(requireContext(), "Orden Realizada", Toast.LENGTH_LONG).show()
                    val bundle = Bundle()
                    bundle.putParcelable("NewO", order)
                    bundle.putString("restaurant", name)
                    it.findNavController().navigate(R.id.action_fragmentPedido_to_succesP, bundle)
                }
            }

        }
        ///--------------- Check Network Connection ------------------------------

        lifecycleScope.launch {
            actividad.conectado.observe(viewLifecycleOwner, {
                val measure = Measure("Pedir orden", DateHelper.dateGenerator(), "")
                if (it) {
                    makeAvailable(binding.button)
                    conectado = true
                    viewModel.initiateMeasure(measure, connected = true)
                } else {
                    makeNetworkAlert(binding.button)
                    conectado = false
                    viewModel.initiateMeasure(measure, connected = false)
                }
            })
        }

        return root
    }

    private fun showDatePickerDialog(fecha: EditText) {
        val newFragment =
            DatePickerFragment.newInstance { _, year, month, day ->
                // +1 because January is zero
                val selectedDate: String = day.toString() + "/" + (month + 1) + "/" + year
                Log.e("Fecha", selectedDate)
                val editable: Editable = SpannableStringBuilder(selectedDate)
                fecha.text = editable
                order.diaPedido = selectedDate
            }

        newFragment.show(requireActivity().supportFragmentManager, "datePicker")
    }

    private var snackbar: Snackbar? = null
    private fun makeNetworkAlert(button: Button) {
        snackbar?.dismiss()
        snackbar = Snackbar.make(button, "Sin conexión", Snackbar.LENGTH_INDEFINITE)
        snackbar?.show()
        button.isEnabled = false
    }

    private fun makeAvailable(button: Button) {
        if (snackbar != null) {
            snackbar?.dismiss()
            snackbar =
                Snackbar.make(button, "Conexión a internet establecida", Snackbar.LENGTH_SHORT)
            snackbar?.show()

        } else {
            snackbar =
                Snackbar.make(button, "Conexión a internet establecida", Snackbar.LENGTH_SHORT)
        }
        button.isEnabled = true
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onStop() {
        viewModel.endMeasure(viewModel.tokenId.value!!,
            DateHelper.dateGenerator(),
            connected = conectado!!)
        super.onStop()
    }

}