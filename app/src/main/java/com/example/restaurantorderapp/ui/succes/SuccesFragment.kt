package com.example.restaurantorderapp.ui.succes

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.example.restaurantorderapp.R
import com.example.restaurantorderapp.activity.home.NetworkStatus
import com.example.restaurantorderapp.activity.home.NetworkStatusHelper
import com.example.restaurantorderapp.databinding.FragmentSuccesBinding
import com.example.restaurantorderapp.model.Reserva
import com.google.android.material.snackbar.Snackbar

class SuccesFragment : Fragment() {
    private var _binding: FragmentSuccesBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _binding = FragmentSuccesBinding.inflate(inflater, container, false)
        val root: View = binding.root
        //bundle to obtain New reserva
        val reserva = arguments?.getParcelable<Reserva>("New")
        val restaurante = arguments?.getString("restaurant")
        //Setting info
        binding.txtNameR.text = restaurante
        if (reserva != null) {
            binding.txtDiaR.text = "Dia Reserva: "+reserva.diaReserva
            binding.txtHoraR.text = "Hora Reserva: "+reserva.horaReserva
            binding.txtPuestosR.text = "Puestos: "+reserva.puestos
            var platos = ""
            val list = reserva.platos
            for (el in list)
            {
               platos += ",$el"
            }
            binding.txtPlatos.text = "Platos: "+platos
        }

        //back Button
        binding.buttonBack.setOnClickListener {
            it.findNavController().navigate(R.id.action_succesFragment_to_nav_home)
        }


        ///--------------- Check Network Connection ------------------------------

        NetworkStatusHelper(requireContext()).observe(viewLifecycleOwner, {
            when(it){
                NetworkStatus.Available -> makeAvailable(binding.buttonBack)
                NetworkStatus.Unavailable -> makeNetworkAlert(binding.buttonBack)
            }
        })

        return root
    }
    private var snackbar: Snackbar?=null
    private fun makeNetworkAlert(button: Button) {
        snackbar?.dismiss()
        snackbar= Snackbar.make(button,"Sin conexión", Snackbar.LENGTH_INDEFINITE)
        snackbar?.show()
    }

    private fun makeAvailable(button: Button) {
        if(snackbar!= null)
        {
            snackbar?.dismiss()
            snackbar = Snackbar.make(button,"Conexión a internet establecida", Snackbar.LENGTH_SHORT)
            snackbar?.show()

        }
        else
        {
            snackbar = Snackbar.make(button,"Conexión a internet establecida", Snackbar.LENGTH_SHORT)
        }
    }

}
