package com.example.restaurantorderapp.ui.listarReservas

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.restaurantorderapp.R
import com.example.restaurantorderapp.activity.home.CustomAdapterReserva
import com.example.restaurantorderapp.activity.home.HomeActivity
import com.example.restaurantorderapp.databinding.FragmentSlideshowBinding
import com.example.restaurantorderapp.model.Cliente
import com.example.restaurantorderapp.model.DateHelper
import com.example.restaurantorderapp.model.Measure
import com.example.restaurantorderapp.model.Reserva
import com.google.gson.Gson
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class listarReserva : Fragment() {

    private lateinit var actividad: HomeActivity
    private lateinit var sharedRef: SharedPreferences
    private lateinit var viewModel: listarReservaViewModel
    private var _binding: FragmentSlideshowBinding? = null
    private val gson = Gson()
    private val USER_DATA = "userdata"
    private var conectado: Boolean? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {

        actividad = activity as HomeActivity
        val repository = actividad.repository
        sharedRef = requireActivity().getPreferences(Context.MODE_PRIVATE)
        val viewModelFactory = listarReservaViewModelFactory(repository)
        viewModel =
            ViewModelProvider(this, viewModelFactory)[listarReservaViewModel::class.java]

        _binding = FragmentSlideshowBinding.inflate(inflater, container, false)
        val root: View = binding.root
        //Bandle to obtain id of current user
        val data = sharedRef.getString(USER_DATA, "NaN")
        val cliente = gson.fromJson(data, Cliente::class.java)
        //Recycler View
        var reservaList: MutableList<Reserva>

        actividad.conectado.observe(viewLifecycleOwner, {
            val measure = Measure("Listar reservas", DateHelper.dateGenerator(), "")
            if (it) {
                conectado=true
                viewModel.getReservas(cliente.id, true)
                viewModel.initiateMeasure(measure, connected = true)
            } else {
                conectado=false
                viewModel.getReservas(cliente.id, false)
                viewModel.initiateMeasure(measure, connected = false)
            }
        })


        lifecycleScope.launch(Dispatchers.Main)
        {
            val reservaObserver = Observer<List<Reserva>> { reservas ->
                reservaList = reservas as MutableList<Reserva>
                val rcv_reserva = binding.rcvReserva
                val adapter = CustomAdapterReserva(reservaList)
                rcv_reserva.layoutManager =
                    LinearLayoutManager(context, RecyclerView.VERTICAL, false)
                rcv_reserva.adapter = adapter
                adapter.setOnItemClickListener(object : CustomAdapterReserva.ClickListener {
                    override fun onClick(pos: Int, aView: View) {
                        Toast.makeText(context, reservaList[pos].id, Toast.LENGTH_SHORT).show()
                        val bundle = Bundle()
                        bundle.putParcelable("New", reservaList[pos])
                        aView.findNavController()
                            .navigate(R.id.action_nav_reservation_history_to_succesFragment, bundle)
                    }
                })
            }
            viewModel.reservas.observe(viewLifecycleOwner, reservaObserver)
        }
        ///--------------- Check Network Connection ------------------------------

        return root
    }



    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onStop() {
        viewModel.endMeasure(viewModel.tokenId.value,DateHelper.dateGenerator(), connected = conectado!!)
        super.onStop()
    }
}