package com.example.restaurantorderapp.ui.home


import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.restaurantorderapp.R
import com.example.restaurantorderapp.activity.home.CustomAdapter
import com.example.restaurantorderapp.activity.home.HomeActivity
import com.example.restaurantorderapp.databinding.FragmentHomeBinding
import com.example.restaurantorderapp.model.DateHelper
import com.example.restaurantorderapp.model.Measure
import com.example.restaurantorderapp.model.Restaurante
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.*

class HomeFragment : Fragment() {

    private lateinit var userCoord: String


    private val USER_COORD = "usercoord"

    private lateinit var actividad: HomeActivity
    private lateinit var sharedRef: SharedPreferences
    private lateinit var homeViewModel: HomeViewModel
    private var _binding: FragmentHomeBinding? = null
    private var conectado: Boolean? = null


    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        actividad = activity as HomeActivity
        super.onCreateView(inflater, container, savedInstanceState)
        sharedRef = requireActivity().getPreferences(Context.MODE_PRIVATE)
        val repository = actividad.repository
        val viewModelFactory = HomeViewModelFactory(repository)
        homeViewModel = ViewModelProvider(this, viewModelFactory)[HomeViewModel::class.java]
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val root: View = binding.root
        var restauranteList: MutableList<Restaurante>


        lifecycleScope.launch {
            val measure = Measure("Listar restaurantes", DateHelper.dateGenerator(), "")
            actividad = activity as HomeActivity
            actividad.conectado.observe(viewLifecycleOwner, {
                if (it) {
                    Log.e("REQUEST", "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^CONECTADO")
                    homeViewModel.getRestaurantes(actividad.cliente, true)
                    makeAvailable(binding.btnRaiting,
                        binding.btnRecomendacion,
                        binding.btnUbicacion)
                    homeViewModel.initiateMeasure(measure, connected = true)
                    conectado = true
                } else {
                    Log.e("CACHE", "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^NO  CONECTADO")
                    homeViewModel.getRestaurantes(actividad.cliente, false)
                    markButtonDisable(binding.btnRaiting,
                        binding.btnRecomendacion,
                        binding.btnUbicacion)
                    homeViewModel.initiateMeasure(measure, connected = false)
                    conectado = false
                }
            })
        }

        lifecycleScope.launch(Dispatchers.Main) {

            userCoord = async(Dispatchers.IO) {
                return@async sharedRef.getString(USER_COORD,
                    "NaN")
            }.await()!!
            Log.e("DEBUG", userCoord)
            val restaurantObserver = Observer<List<Restaurante>> { restaurantes ->
                restauranteList = restaurantes as MutableList<Restaurante>
                val rcv_restaurantes = binding.rcvRestaurantes
                val adapter = CustomAdapter(restauranteList, userCoord)

                rcv_restaurantes.layoutManager =
                    LinearLayoutManager(context, RecyclerView.VERTICAL, false)
                rcv_restaurantes.adapter = adapter

                adapter.setOnItemClickListener(object : CustomAdapter.ClickListener {
                    override fun onClick(pos: Int, aView: View) {

                        val restaurante: Restaurante = restauranteList[pos]
                        val bundle = Bundle()
                        bundle.putParcelable("selected", restaurante)
                        aView.findNavController()
                            .navigate(R.id.action_nav_home_to_botonesReservaPedido, bundle)

                    }
                })
            }
            homeViewModel.restaurantes.observe(viewLifecycleOwner, restaurantObserver)
        }

        binding.btnRaiting.setOnClickListener {
            homeViewModel.getRestaurantesByRating()
        }

        binding.btnRecomendacion.setOnClickListener {
            homeViewModel.getRecomendacionesRestaurantes(actividad.cliente, conectado!!)
        }

        binding.btnUbicacion.setOnClickListener {
            CoroutineScope(Dispatchers.IO).launch {
                val userCoord = async { return@async sharedRef.getString(USER_COORD, "NaN") }
                homeViewModel.getRestaurantesByDistance(userCoord.await()!!)
            }
        }
        ///--------------- Check Network Connection ------------------------------
        return root
    }


    private fun markButtonDisable(loginButton: Button, button1: Button, button2: Button) {
        loginButton.isEnabled = false
        button1.isEnabled = false
        button2.isEnabled = false

    }

    private fun markButtonAvailable(loginButton: Button, button1: Button, button2: Button) {
        loginButton.isEnabled = true
        button1.isEnabled = true
        button2.isEnabled = true
        //button.setTextColor(ContextCompat.getColor(textView.context, R.color.Yellow))
        //button.setBackgroundColor(ContextCompat.getColor(textView.context, R.color.Light_Text_Color))
    }

    private var snackbar: Snackbar? = null
    private fun makeAvailable(loginButton: Button, button1: Button, button2: Button) {
        markButtonAvailable(loginButton, button1, button2)
        if (snackbar != null) {
            snackbar?.dismiss()
            snackbar =
                Snackbar.make(loginButton,
                    "Conexión a internet establecida",
                    Snackbar.LENGTH_SHORT)
            snackbar?.show()

        } else {
            snackbar =
                Snackbar.make(loginButton,
                    "Conexión a internet establecida",
                    Snackbar.LENGTH_SHORT)
        }
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onStop() {
        homeViewModel.endMeasure(homeViewModel.tokenId.value,DateHelper.dateGenerator(), connected = conectado!!)
        super.onStop()
    }

}
