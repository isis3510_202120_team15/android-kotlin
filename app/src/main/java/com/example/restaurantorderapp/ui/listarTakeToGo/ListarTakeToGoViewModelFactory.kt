package com.example.restaurantorderapp.ui.listarTakeToGo

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.restaurantorderapp.api.Repository

class ListarTakeToGoViewModelFactory(private val repository: Repository):  ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return ListarTakeToGoViewModel(repository) as T
    }
}