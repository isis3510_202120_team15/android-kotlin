package com.example.restaurantorderapp.ui.listarReservas

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.restaurantorderapp.api.Repository
import com.example.restaurantorderapp.model.Measure
import com.example.restaurantorderapp.model.Message
import com.example.restaurantorderapp.model.Reserva
import kotlinx.coroutines.launch


class listarReservaViewModel(private val repository: Repository) : ViewModel() {
    var reservas: MutableLiveData<List<Reserva>> = MutableLiveData()
    var tokenId: MutableLiveData<String> = MutableLiveData()

    fun getReservas(idCliente: String, connected: Boolean) {
        viewModelScope.launch {
            val response = repository.getReservaByClientId(idCliente, connected)
            if (response != null) reservas.postValue(response.body())
            else reservas.postValue(mutableListOf())
        }
    }

    fun initiateMeasure(measure: Measure, connected: Boolean) {
        viewModelScope.launch {
            if (connected) {
                val response = repository.initiateMeasure(measure,connected)
                if (response?.isSuccessful==true) {
                    tokenId.setValue(response.body()!!.tokenId)
                } else {
                    Log.e("WATCHDOG", "ERROR SENDIND TOKEN")
                }
            }
        }
    }


    fun endMeasure(tokenId: String?, endHour: String, connected: Boolean) {
        if (connected) {
            viewModelScope.launch {
                repository.endMeasure(tokenId, Message(endHour),connected)
            }
        }
    }
}

