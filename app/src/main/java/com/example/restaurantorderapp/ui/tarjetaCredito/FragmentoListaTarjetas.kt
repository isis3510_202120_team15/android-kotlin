package com.example.restaurantorderapp.ui.tarjetaCredito

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.restaurantorderapp.R
import com.example.restaurantorderapp.activity.home.CustomAdapterTarjeta
import com.example.restaurantorderapp.activity.home.HomeActivity
import com.example.restaurantorderapp.databinding.TarjetasFragmentBinding
import com.example.restaurantorderapp.model.Cliente
import com.example.restaurantorderapp.model.DateHelper
import com.example.restaurantorderapp.model.Measure
import com.example.restaurantorderapp.model.Tarjeta
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class FragmentoListaTarjetas : Fragment() {


    private lateinit var actividad: HomeActivity
    private lateinit var sharedRef: SharedPreferences
    private lateinit var tarjetaViewModel: TarjetaViewModel
    private var _binding: TarjetasFragmentBinding? = null
    private val binding get() = _binding!!
    private val USER_DATA = "userdata"
    val PREFERED_CARD = "creditcard"
    private var conectado: Boolean? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {

        actividad = activity as HomeActivity
        val repository = actividad.repository
        sharedRef = requireActivity().getPreferences(Context.MODE_PRIVATE)
        val viewModelFactory = TarjetaViewModelFactory(repository)
        tarjetaViewModel = ViewModelProvider(this, viewModelFactory)[TarjetaViewModel::class.java]
        val cliente = Cliente.createFromJSON(sharedRef.getString(USER_DATA, "{}")!!)
        val selectedCreditCard = sharedRef.getString(PREFERED_CARD, "{}")

        _binding = TarjetasFragmentBinding.inflate(inflater, container, false)
        val root: View = binding.root
        var cardList: MutableList<Tarjeta>


        lifecycleScope.launch(Dispatchers.Main) {
            val tarjetasObserver = Observer<List<Tarjeta>> { tarjetas ->
                cardList = tarjetas as MutableList<Tarjeta>
                val rcv_tarjetas = binding.rcvTarjetas
                val adapter = CustomAdapterTarjeta(cardList, selectedCreditCard!!)
                rcv_tarjetas.layoutManager =
                    LinearLayoutManager(context, RecyclerView.VERTICAL, false)
                rcv_tarjetas.adapter = adapter
                adapter.setOnItemClickListener(object : CustomAdapterTarjeta.ClickListener {
                    override fun onClick(pos: Int, aView: View) {
                        val tarjeta = cardList[pos]
                        with(sharedRef.edit()) {
                            putString(PREFERED_CARD, tarjeta.id)
                            apply()
                        }
                    }
                })
            }
            tarjetaViewModel.tarjetas.observe(viewLifecycleOwner, tarjetasObserver)
        }


        binding.btnAddTarjeta.setOnClickListener {
            it.findNavController().navigate(R.id.add_tarjeta)
        }


        lifecycleScope.launch {
            actividad.conectado.observe(viewLifecycleOwner, {
                val measure = Measure("Listar tarjetas", DateHelper.dateGenerator(), "")
                if (it) {
                    tarjetaViewModel.getTarjetas(cliente, true)
                    makeAvailable(binding.btnAddTarjeta)
                    conectado = true
                    tarjetaViewModel.initiateMeasure(measure, connected = true)
                } else {
                    tarjetaViewModel.getTarjetas(cliente, false)
                    makeNetworkAlert(binding.btnAddTarjeta)
                    conectado = false
                    tarjetaViewModel.initiateMeasure(measure, connected = false)
                }
            })
        }

        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onStop() {
        tarjetaViewModel.endMeasure(tarjetaViewModel.tokenId.value,
            DateHelper.dateGenerator(),
            connected = conectado!!)
        super.onStop()
    }

    private var alert: AlertDialog? = null
    private fun makeAvailable(button1: FloatingActionButton) {
        alert?.cancel()
        markButtonAvailable(button1)
        Toast.makeText(requireContext(), "Conexion a internet establecida", Toast.LENGTH_SHORT)
            .show()
    }

    private fun makeNetworkAlert(button1: FloatingActionButton) {
        markButtonDisable(button1)
        val dialogBuilder = AlertDialog.Builder(requireContext())
        // set message of alert dialog
        dialogBuilder.setMessage("Aun así, puede seguir usando algunas funcionalidades. Para volver " +
                "a tener acceso a todas las funcionalidades por favor revise su conexión a internet")
            // if the dialog is cancelable
            .setCancelable(true)
            // positive button text and action
            .setPositiveButton("Ok") { _, _ ->
            }

        // create dialog box
        alert = dialogBuilder.create()
        // set title for alert dialog box
        alert?.setTitle("No hay conexión a internet")
        alert?.setIcon(R.mipmap.ic_launcher)
        // show alert dialog
        alert?.show()

    }

    private fun markButtonDisable(button1: FloatingActionButton) {
        button1.isEnabled = false
    }

    private fun markButtonAvailable(button1: FloatingActionButton) {
        button1.isEnabled = true
    }
}
