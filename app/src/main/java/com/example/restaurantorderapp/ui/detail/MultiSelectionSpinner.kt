package com.example.restaurantorderapp.ui.detail

import android.R.layout
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.DialogInterface.OnMultiChoiceClickListener
import android.util.AttributeSet
import android.widget.ArrayAdapter
import androidx.appcompat.widget.AppCompatSpinner
import java.lang.IllegalArgumentException
import android.widget.SpinnerAdapter
import com.example.restaurantorderapp.model.Plato
import java.lang.RuntimeException
import java.lang.StringBuilder
import java.util.*


class MultiSelectionSpinner : AppCompatSpinner, OnMultiChoiceClickListener {
    private var items: Array<Plato> = arrayOf(Plato("","",0))
    private var selection: BooleanArray = booleanArrayOf(false)
    var adapter: ArrayAdapter<String>

    constructor(context: Context?) : super(context!!) {
        adapter = ArrayAdapter<String>(context,
            layout.simple_spinner_item)
        super.setAdapter(adapter)
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context!!, attrs) {
        adapter = ArrayAdapter<String>(context,
            layout.simple_spinner_item)
        super.setAdapter(adapter)
    }

    override fun onClick(dialog: DialogInterface?, idx: Int, isChecked: Boolean) {
        if (idx < selection.size) {
            selection[idx] = isChecked
            adapter.clear()
            adapter.add(buildSelectedItemString())
        } else {
            throw IllegalArgumentException(
                "'idx' is out of bounds.")
        }
    }
    override fun performClick(): Boolean {
        super.performClick()
        val builder: AlertDialog.Builder = AlertDialog.Builder(context)
        val itemNames = arrayOfNulls<String>(items.size)
        for (i in items.indices) {
            itemNames[i] = items[i].nombre
        }
        builder.setMultiChoiceItems(itemNames, selection, this)
        builder.setPositiveButton("OK") { _, _ ->
            // Do nothing
        }
        builder.show()
        return true
    }
    override fun setAdapter(adapter: SpinnerAdapter?) {
        throw RuntimeException(
            "setAdapter is not supported by MultiSelectSpinner.")
    }
    @JvmName("setItems1")
    fun setItems(items: Array<Plato>) {
        this.items = items
        selection = BooleanArray(this.items.size)
        adapter.clear()
        adapter.add("")
        Arrays.fill(selection, false)
    }
    override fun setSelection(position: Int) {
        for (i in this.selection.indices) {
            this.selection[i] = false
        }

        adapter.clear()
        adapter.add(buildSelectedItemString())
    }
    fun getSelectedItems(): Array<String> {
        val selectedItems: MutableList<String> = emptyArray<String>().toMutableList()
        for (i in items.indices) {
            if (selection[i]) {
                selectedItems.add(items[i].id)
            }
        }
        return selectedItems.toTypedArray()
    }
    private fun buildSelectedItemString(): String {
        val sb = StringBuilder()
        var foundOne = false
        for (i in items.indices) {
            if (selection[i]) {
                if (foundOne) {
                    sb.append(", ")
                }
                foundOne = true
                sb.append(items[i])
            }
        }
        return sb.toString()
    }
}