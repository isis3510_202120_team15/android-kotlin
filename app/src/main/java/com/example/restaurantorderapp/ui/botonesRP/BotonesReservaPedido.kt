package com.example.restaurantorderapp.ui.botonesRP


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.navigation.findNavController
import com.example.restaurantorderapp.R
import com.example.restaurantorderapp.activity.home.NetworkStatus
import com.example.restaurantorderapp.activity.home.NetworkStatusHelper
import com.example.restaurantorderapp.databinding.BotonesReservaPedidoFragmentBinding
import com.example.restaurantorderapp.model.Restaurante
import com.google.android.material.snackbar.Snackbar

class BotonesReservaPedido : Fragment() {


    private var _binding: BotonesReservaPedidoFragmentBinding? = null
    private val binding get() = _binding!!
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _binding = BotonesReservaPedidoFragmentBinding.inflate(inflater, container, false)
        val root: View = binding.root

        val selectedR = arguments?.getParcelable<Restaurante>("selected")!!
        val bundle = Bundle()
        bundle.putParcelable("selected",selectedR)
        binding.buttonReservar.setOnClickListener {
            it.findNavController().navigate(R.id.action_botonesReservaPedido_to_detail,bundle)
        }
        binding.buttonPedir.setOnClickListener {
            it.findNavController().navigate(R.id.action_botonesReservaPedido_to_fragmentPedido,bundle)
        }
        ///--------------- Check Network Connection ------------------------------
        NetworkStatusHelper(requireContext()).observe(viewLifecycleOwner, {
            when(it){
                NetworkStatus.Available -> makeAvailable(binding.buttonReservar,binding.buttonPedir)
                NetworkStatus.Unavailable -> makeNetworkAlert(binding.buttonReservar,binding.buttonPedir)
            }
        })
        return root
    }
    private var snackbar:Snackbar?=null
    private fun makeNetworkAlert(button: Button,button2: Button) {
        snackbar?.dismiss()
        snackbar= Snackbar.make(button,"Sin conexión", Snackbar.LENGTH_INDEFINITE)
        snackbar?.show()
        button.isEnabled = false
        button2.isEnabled = false
    }

    private fun makeAvailable(button: Button,button2: Button) {
        if(snackbar!= null)
        {
            snackbar?.dismiss()
            snackbar = Snackbar.make(button,"Conexión a internet establecida", Snackbar.LENGTH_SHORT)
            snackbar?.show()

        }
        else
        {
            snackbar = Snackbar.make(button,"Conexión a internet establecida", Snackbar.LENGTH_SHORT)
        }
        button.isEnabled =true
        button2.isEnabled=true
    }


}