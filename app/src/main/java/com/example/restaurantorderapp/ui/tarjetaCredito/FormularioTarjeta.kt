package com.example.restaurantorderapp.ui.tarjetaCredito

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import com.example.restaurantorderapp.R
import com.example.restaurantorderapp.activity.home.HomeActivity
import com.example.restaurantorderapp.databinding.AddTarjetaBinding
import com.example.restaurantorderapp.model.Cliente
import com.example.restaurantorderapp.model.DateHelper
import com.example.restaurantorderapp.model.Measure
import com.example.restaurantorderapp.model.Tarjeta
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.*

class FormularioTarjeta : Fragment() {
    private lateinit var nombre: String
    private lateinit var numero: String

    private lateinit var sharedRef: SharedPreferences
    private lateinit var tarjetaViewModel: TarjetaViewModel
    private var _binding: AddTarjetaBinding? = null
    private val binding get() = _binding!!
    private val USER_DATA = "userdata"
    private var conectado: Boolean? = null
    private lateinit var actividad: HomeActivity

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        actividad = activity as HomeActivity
        val repository = actividad.repository
        sharedRef = requireActivity().getPreferences(Context.MODE_PRIVATE)
        val cliente = Cliente.createFromJSON(sharedRef.getString(USER_DATA, "{}")!!)
        val viewModelFactory = TarjetaViewModelFactory(repository)
        tarjetaViewModel = ViewModelProvider(this, viewModelFactory)[TarjetaViewModel::class.java]
        _binding = AddTarjetaBinding.inflate(inflater, container, false)
        val root: View = binding.root

        binding.txtMes.text
        binding.btnAddTarjeta.setOnClickListener {
            val month = binding.txtMes.text.toString()
            val anio = binding.txtAnio.text.toString()
            nombre = binding.txtNombre.text.toString()
            numero = binding.txtNumber.text.toString()
            if (checkearDatos(numero, month, anio, nombre)) {

                val tarjeta = Tarjeta(null,numero, nombre, "${month}/${anio}")
                lifecycleScope.launch(Dispatchers.IO) {
                    tarjetaViewModel.postTarjeta(cliente.id, tarjeta)
                    withContext(Dispatchers.Main) {
                        Toast.makeText(requireContext(), "Tarjeta Añadida", Toast.LENGTH_LONG)
                            .show()
                    }
                }
                it.findNavController().navigate(R.id.nav_payment_methods)
            }
        }

        ///--------------- Check Network Connection ------------------------------
        lifecycleScope.launch {
            actividad.conectado.observe(viewLifecycleOwner, {
                val measure = Measure("Crear tarjeta", DateHelper.dateGenerator(), "")
                if (it) {
                    makeAvailable(binding.btnAddTarjeta)
                    conectado = true
                    tarjetaViewModel.initiateMeasure(measure, connected = true)
                } else {
                    makeNetworkAlert(binding.btnAddTarjeta)
                    conectado = false
                    tarjetaViewModel.initiateMeasure(measure, connected = false)
                }
            })
        }


        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onStop() {
        tarjetaViewModel.endMeasure(tarjetaViewModel.tokenId.value,
            DateHelper.dateGenerator(),
            connected = conectado!!)
        super.onStop()
    }



    private var snackbar: Snackbar? = null
    private fun makeAvailable(add: Button) {
        add.isEnabled = true
        if (snackbar != null) {
            snackbar?.dismiss()
            snackbar = Snackbar.make(add, "Conexión a internet establecida", Snackbar.LENGTH_SHORT)
            snackbar?.show()

        } else {
            snackbar = Snackbar.make(add, "Conexión a internet establecida", Snackbar.LENGTH_SHORT)
        }
    }

    private fun makeNetworkAlert(add: Button) {
        add.isEnabled = false
        snackbar?.dismiss()
        snackbar = Snackbar.make(add, "Sin conexión", Snackbar.LENGTH_INDEFINITE)
        snackbar?.show()
    }

    private fun checkearDatos(number: String, month: String, anio: String, nombre: String): Boolean {
        var resp = true
        if (nombre.isBlank() || number.isBlank() || month.isBlank() || anio.isBlank()) {
            Toast.makeText(requireContext(), "hay campos en blanco", Toast.LENGTH_SHORT).show()
            return false
        } else if (number.length > 16 || number.length < 16) {
            //toast
            Toast.makeText(requireContext(), "El numero debe tener 16 digitos", Toast.LENGTH_SHORT)
                .show()
            resp = false
        } else if (month.length > 2 || month.toInt() > 12) {
            Toast.makeText(requireContext(),
                "El mes no puede tener mas de 2 digitos y debe ser menor de 12",
                Toast.LENGTH_SHORT).show()
            resp = false
        } else if (anio.length > 4 || anio.length < 4) {
            Toast.makeText(requireContext(), "El año debe tener 4 digitos", Toast.LENGTH_SHORT)
                .show()
            resp = false
        }
        val fecha = Calendar.getInstance()
        fecha.set(Calendar.MONTH, month.toInt())
        fecha.set(Calendar.DAY_OF_MONTH, fecha.getMaximum(Calendar.MONTH))
        fecha.set(Calendar.YEAR, anio.toInt())


        val today: Calendar = Calendar.getInstance()
        today.set(Calendar.DAY_OF_MONTH, 1)
        today.set(Calendar.MONTH, today.get(Calendar.MONTH) + 1)
        Log.e("FECHAAAAAAAAAAA^^^^^^", fecha.toString())
        Log.e("FECHAAAAAA", today.toString())

        if (fecha.before(today)) {
            //toast
            Toast.makeText(requireContext(),
                "La fecha no puede ser anterior a la actual",
                Toast.LENGTH_SHORT).show()
            resp = false
        }
        return resp
    }

}