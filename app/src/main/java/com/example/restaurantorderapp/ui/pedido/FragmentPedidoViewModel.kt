package com.example.restaurantorderapp.ui.pedido

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.restaurantorderapp.api.Repository
import com.example.restaurantorderapp.model.Measure
import com.example.restaurantorderapp.model.Message
import com.example.restaurantorderapp.model.TakeToGoOrder
import kotlinx.coroutines.launch

class FragmentPedidoViewModel(private val repository: Repository) : ViewModel() {

    var tokenId: MutableLiveData<String> = MutableLiveData()

    fun postOrder(order: TakeToGoOrder)
    {
        viewModelScope.launch {
            repository.createTakeToGoOrder(order)
        }
    }

    fun initiateMeasure(measure: Measure, connected: Boolean) {
        viewModelScope.launch {
            if (connected) {
                val response = repository.initiateMeasure(measure,connected)
                if (response?.isSuccessful==true) {
                    tokenId.setValue(response.body()!!.tokenId)
                } else {
                    Log.e("WATCHDOG", "ERROR SENDIND TOKEN")
                }
            }
        }
    }


    fun endMeasure(tokenId: String?, endHour: String, connected: Boolean) {
        if (connected) {
            viewModelScope.launch {
                repository.endMeasure(tokenId, Message(endHour),connected)
            }
        }
    }
}