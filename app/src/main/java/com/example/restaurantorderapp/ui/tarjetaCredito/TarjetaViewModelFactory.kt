package com.example.restaurantorderapp.ui.tarjetaCredito

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.restaurantorderapp.api.Repository


class TarjetaViewModelFactory(private val repository: Repository) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return TarjetaViewModel(repository) as T
    }


}