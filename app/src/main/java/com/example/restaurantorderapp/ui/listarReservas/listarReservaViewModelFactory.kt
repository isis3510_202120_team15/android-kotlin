package com.example.restaurantorderapp.ui.listarReservas

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.restaurantorderapp.api.Repository


class listarReservaViewModelFactory(private val repository: Repository):  ViewModelProvider.Factory {


    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return listarReservaViewModel(repository) as T
    }

}
