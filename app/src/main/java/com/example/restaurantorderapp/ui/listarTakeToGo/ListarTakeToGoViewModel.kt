package com.example.restaurantorderapp.ui.listarTakeToGo

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.restaurantorderapp.api.Repository
import com.example.restaurantorderapp.model.*
import kotlinx.coroutines.launch

class ListarTakeToGoViewModel(private val repository: Repository) : ViewModel() {
    var taketogo: MutableLiveData<List<TakeToGoOrder>> = MutableLiveData()
    var tokenId: MutableLiveData<String> = MutableLiveData()

    fun getPedidos(idCliente: String, connected: Boolean) {
        viewModelScope.launch {
            val response = repository.getTakeToGoByClientId(idCliente, connected)
            if(response!=null) taketogo.postValue(response.body())
            else taketogo.postValue(mutableListOf())
        }
    }

    fun initiateMeasure(measure: Measure, connected: Boolean) {
        viewModelScope.launch {
            if (connected) {
                val response = repository.initiateMeasure(measure,connected)
                if (response?.isSuccessful==true) {
                    tokenId.setValue(response.body()!!.tokenId)
                } else {
                    Log.e("WATCHDOG", "ERROR SENDIND TOKEN")
                }
            }
        }
    }


    fun endMeasure(tokenId: String?, endHour: String, connected: Boolean) {
        if (connected && tokenId.isNullOrEmpty()) {
            viewModelScope.launch {
                repository.endMeasure(tokenId, Message(endHour),connected)
                Log.e("WATCHDOG", "^^^^^^^SENT INFO. TOKENID${tokenId}")
            }
        }
    }
}