package com.example.restaurantorderapp.ui.home

import androidx.lifecycle.ViewModel
import com.example.restaurantorderapp.api.Repository
import androidx.lifecycle.ViewModelProvider


class HomeViewModelFactory(private val repository: Repository) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return HomeViewModel(repository) as T
    }
}