package com.example.restaurantorderapp.ui.succes

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.example.restaurantorderapp.R
import com.example.restaurantorderapp.databinding.FragmentSuccesPBinding
import com.example.restaurantorderapp.model.TakeToGoOrder


class SuccesP : Fragment() {
    private var _binding: FragmentSuccesPBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _binding = FragmentSuccesPBinding.inflate(inflater, container, false)
        val root: View = binding.root
        //bundle to obtain New order
        val order = arguments?.getParcelable<TakeToGoOrder>("NewO")
        val restaurante = arguments?.getString("restaurant")
        //Setting info
        binding.txtNameRP.text = restaurante
        if (order != null) {
            binding.txtDiaRP.text = "Dia Reserva: "+order.diaPedido
            binding.txtHoraRP.text = "Hora Reserva: "+order.horaPedido
            var platos = ""
            val list = order.platos
            for (el in list)
            {
                platos += ",$el"
            }
            binding.txtPlatosP.text = "Platos: "+platos
        }

        //back Button
        binding.buttonBackP.setOnClickListener {
            it.findNavController().navigate(R.id.action_succesP_to_nav_home2)
        }


        return root
    }

}