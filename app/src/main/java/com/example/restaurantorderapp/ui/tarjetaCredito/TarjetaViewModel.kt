package com.example.restaurantorderapp.ui.tarjetaCredito

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.restaurantorderapp.api.Repository
import com.example.restaurantorderapp.model.Cliente
import com.example.restaurantorderapp.model.Measure
import com.example.restaurantorderapp.model.Message
import com.example.restaurantorderapp.model.Tarjeta
import kotlinx.coroutines.launch

class TarjetaViewModel(private val repository: Repository) : ViewModel() {

    var tarjetas: MutableLiveData<List<Tarjeta>> = MutableLiveData()
    var tokenId: MutableLiveData<String> = MutableLiveData()

    fun getTarjetas(cliente: Cliente, connected: Boolean) {
        viewModelScope.launch {
            val response = repository.getTarjetas(cliente.id, connected)
            if (response != null) tarjetas.postValue(response.body())
            else tarjetas.postValue(mutableListOf())
        }
    }

    fun postTarjeta(idCliente: String, tarjeta: Tarjeta) {
        viewModelScope.launch {
            repository.createTarjeta(idCliente, tarjeta)
        }
    }

    fun initiateMeasure(measure: Measure, connected: Boolean) {
        viewModelScope.launch {
            if (connected) {
                val response = repository.initiateMeasure(measure,connected)
                if (response?.isSuccessful==true) {
                    tokenId.value = response.body()!!.tokenId
                }
            }
        }
    }


    fun endMeasure(tokenId: String?, endHour: String, connected: Boolean) {
        if (connected) {
            viewModelScope.launch {
                repository.endMeasure(tokenId, Message(endHour),connected)
            }
        }
    }

}