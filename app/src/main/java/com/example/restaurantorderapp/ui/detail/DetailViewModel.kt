package com.example.restaurantorderapp.ui.detail

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.restaurantorderapp.api.Repository
import com.example.restaurantorderapp.model.Measure
import com.example.restaurantorderapp.model.Message
import com.example.restaurantorderapp.model.Reserva
import kotlinx.coroutines.launch

class DetailViewModel(private val repository: Repository) : ViewModel() {

    var tokenId: MutableLiveData<String> = MutableLiveData()


    fun postReserva(reserva:Reserva)
    {
        viewModelScope.launch {
            repository.createReserva(reserva)
        }
    }

    fun initiateMeasure(measure: Measure, connected: Boolean) {
        viewModelScope.launch {
            if (connected) {
                val response = repository.initiateMeasure(measure,connected)
                if (response?.isSuccessful==true) {
                    tokenId.value = response.body()!!.tokenId
                }
            }
        }
    }


    fun endMeasure(tokenId: String, endHour: String, connected: Boolean) {
        if (connected) {
            viewModelScope.launch {
                repository.endMeasure(tokenId, Message(endHour),connected)
            }
        }
    }

}