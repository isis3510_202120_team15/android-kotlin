package com.example.restaurantorderapp.ui.home

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.restaurantorderapp.api.Repository
import com.example.restaurantorderapp.model.Cliente
import com.example.restaurantorderapp.model.Measure
import com.example.restaurantorderapp.model.Message
import com.example.restaurantorderapp.model.Restaurante
import kotlinx.coroutines.launch

class HomeViewModel(private val repository: Repository) : ViewModel() {

    var restaurantes: MutableLiveData<List<Restaurante>> = MutableLiveData()
    private var todosRestaurantes: MutableLiveData<List<Restaurante>> = MutableLiveData()
    private var recomendaciones: MutableLiveData<List<Restaurante>> = MutableLiveData()
    var tokenId: MutableLiveData<String> = MutableLiveData()

    fun getRestaurantes(cliente: Cliente, connected: Boolean) {
        viewModelScope.launch {
            val response = repository.getAllRestaurantes(connected)
            if (response != null) {
                restaurantes.postValue(response.body())
                todosRestaurantes.postValue(response.body())
            } else {
                restaurantes.postValue(mutableListOf())
                todosRestaurantes.postValue(mutableListOf())
            }
        }
        viewModelScope.launch {
            val response = repository.getRecomendacionesByCliente(cliente.id, connected)
            if (response != null) recomendaciones.postValue(response.body())
            else recomendaciones.postValue(mutableListOf())
        }
    }

    fun getRestaurantesByRating() {
        viewModelScope.launch {
            var response = restaurantes.value!!
            if (todosRestaurantes.value!!.size > restaurantes.value!!.size) {
                response = todosRestaurantes.value!!
            }
            response = (response.sortedBy { it.getRating() }).reversed()
            restaurantes.postValue(response)
        }
    }

    fun getRecomendacionesRestaurantes(cliente: Cliente, connected: Boolean) {
        viewModelScope.launch {
            if (recomendaciones.value.isNullOrEmpty()) {
                val response = repository.getRecomendacionesByCliente(cliente.id, connected)
                if (response != null) recomendaciones.postValue(response.body())
                else recomendaciones.postValue(mutableListOf())
            }
            restaurantes.postValue(recomendaciones.value)
        }
    }


    fun getRestaurantesByDistance(userCoord: String) {
        viewModelScope.launch {
            var response = restaurantes.value!!
            if (todosRestaurantes.value!!.size > restaurantes.value!!.size) {
                response = todosRestaurantes.value!!
            }
            response = (response.sortedBy { it.calculateDistance(userCoord) })
            restaurantes.postValue(response)
        }
    }

    fun initiateMeasure(measure: Measure, connected: Boolean) {
        viewModelScope.launch {
            if (connected) {
                val response = repository.initiateMeasure(measure,connected)
                if (response?.isSuccessful == true) {
                    tokenId.setValue(response.body()!!.tokenId)
                } else {
                    Log.e("WATCHDOG", "ERROR SENDIND TOKEN")
                }
            }
        }
    }


    fun endMeasure(tokenId: String?, endHour: String, connected: Boolean) {
        if (connected) {
            viewModelScope.launch {
                repository.endMeasure(tokenId, Message(endHour), connected)
            }
        }
    }
}