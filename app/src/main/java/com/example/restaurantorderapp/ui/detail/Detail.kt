package com.example.restaurantorderapp.ui.detail

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.text.Editable
import android.text.SpannableStringBuilder
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import com.example.restaurantorderapp.R
import com.example.restaurantorderapp.activity.home.HomeActivity
import com.example.restaurantorderapp.databinding.FragmentDetailBinding
import com.example.restaurantorderapp.model.*
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import kotlinx.coroutines.launch


class Detail : Fragment() {

    private lateinit var sharedRef: SharedPreferences
    private lateinit var viewModel: DetailViewModel
    private var _binding: FragmentDetailBinding? = null
    private val binding get() = _binding!!
    private val gson = Gson()
    private val USER_DATA = "userdata"
    private var conectado: Boolean? = null
    private lateinit var actividad: HomeActivity


    //Doing a reservation
    private lateinit var reserva: Reserva

    //selected restaurant
    private lateinit var selectedR: Restaurante
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        actividad = activity as HomeActivity
        sharedRef = requireActivity().getPreferences(Context.MODE_PRIVATE)
        val repository = actividad.repository
        val ViewModelFactory = DetailViewModelFactory(repository)
        viewModel = ViewModelProvider(this, ViewModelFactory)[DetailViewModel::class.java]
        _binding = FragmentDetailBinding.inflate(inflater, container, false)
        val root: View = binding.root

        //Look for current user
        val data = sharedRef.getString(USER_DATA, "NaN")
        val user = gson.fromJson(data, Cliente::class.java)
        //Look for current restaurant info
        selectedR = arguments?.getParcelable("selected")!!
        val name = selectedR.nombre
        val id: String = selectedR.id
        binding.nameR.text = name
        //creating basic Reserva
        reserva = Reserva("Aa", user.id, id, "", "", emptyArray(), 0)
        //Show available hours of the restaurant
        val hour: Array<String> = selectedR.horariosReserva
        val spinner: Spinner = binding.hourSpiner
        val array_adapter =
            ArrayAdapter(requireContext(), R.layout.support_simple_spinner_dropdown_item, hour)
        array_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner.adapter = array_adapter
        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                println("erreur")
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long,
            ) {
                val type = parent?.getItemAtPosition(position).toString()
                reserva.horaReserva = type
            }
        }
        //setting dates picker
        val fecha: EditText = binding.txtTime
        fecha.setOnClickListener {
            showDatePickerDialog(fecha)
        }
        val multispiner: MultiSelectionSpinner = binding.platSpiner
        val platos: Array<Plato> = selectedR.platos
        multispiner.setItems(platos)
        //Action in button reservar
        val btn_reservar = binding.button
        btn_reservar.setOnClickListener {
            val puestos = binding.txtPuestosE.text.toString()
            val platosS: Array<String> = multispiner.getSelectedItems()
            reserva.platos = platosS
            //entradas vacias
            when {
                TextUtils.isEmpty(puestos) -> {
                    Toast.makeText(
                        requireContext(),
                        "Por favor ingrese el numero de mesas a reservar",
                        Toast.LENGTH_SHORT
                    ).show()
                }
                reserva.platos.isEmpty() -> {
                    Toast.makeText(
                        requireContext(),
                        "Por favor ingrese los platos a pedir",
                        Toast.LENGTH_SHORT
                    ).show()
                }
                reserva.horaReserva === "" -> {
                    Toast.makeText(
                        requireContext(),
                        "Por favor ingrese la hora a reservar",
                        Toast.LENGTH_SHORT
                    ).show()
                }
                reserva.diaReserva === "" -> {
                    Toast.makeText(
                        requireContext(),
                        "Por favor ingrese el dia a reservar",
                        Toast.LENGTH_SHORT
                    ).show()
                }
                else -> {
                    reserva.puestos = puestos.toIntOrNull()!!


                    Log.e("Reserva", reserva.toString())
                    viewModel.postReserva(reserva)
                    Toast.makeText(requireContext(), "Reserva Realizada", Toast.LENGTH_LONG).show()
                    val bundle = Bundle()
                    bundle.putParcelable("New", reserva)
                    bundle.putString("restaurant", name)
                    it.findNavController().navigate(R.id.action_detail_to_succesFragment, bundle)
                }
            }

        }

        ///--------------- Check Network Connection ------------------------------

        lifecycleScope.launch {
            actividad.conectado.observe(viewLifecycleOwner, {
                val measure = Measure("Pedir reserva", DateHelper.dateGenerator(), "")
                if (it) {
                    makeAvailable(binding.button)
                    conectado = true
                    viewModel.initiateMeasure(measure, connected = true)
                } else {
                    makeNetworkAlert(binding.button)
                    conectado = false
                    viewModel.initiateMeasure(measure, connected = false)
                }
            })
        }
        return root
    }

    private fun markButtonDisable(button: Button) {
        button.isEnabled = false
    }

    private fun markButtonAvailable(button: Button) {
        button.isEnabled = true
    }

    private var snackbar: Snackbar? = null
    private fun makeAvailable(loginButton: Button) {
        markButtonAvailable(loginButton)
        snackbar?.dismiss()
        snackbar =
            Snackbar.make(loginButton, "Conexion a internet establecida", Snackbar.LENGTH_SHORT)
        snackbar?.show()
    }

    private fun makeNetworkAlert(loginButton: Button) {
        snackbar?.dismiss()
        snackbar = Snackbar.make(loginButton, "Sin conexion", Snackbar.LENGTH_INDEFINITE)
        snackbar?.show()
        markButtonDisable(loginButton)
    }

    private fun showDatePickerDialog(fecha: EditText) {
        val newFragment =
            DatePickerFragment.newInstance { _, year, month, day ->
                // +1 because January is zero
                val selectedDate: String = day.toString() + "/" + (month + 1) + "/" + year
                Log.e("Fecha", selectedDate)
                val editable: Editable = SpannableStringBuilder(selectedDate)
                fecha.text = editable
                reserva.diaReserva = selectedDate
            }

        newFragment.show(requireActivity().supportFragmentManager, "datePicker")
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onStop() {
        viewModel.endMeasure(viewModel.tokenId.value!!,
            DateHelper.dateGenerator(),
            connected = conectado!!)
        super.onStop()
    }


}

