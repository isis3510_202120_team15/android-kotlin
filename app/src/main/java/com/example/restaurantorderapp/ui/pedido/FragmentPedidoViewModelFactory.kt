package com.example.restaurantorderapp.ui.pedido

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.restaurantorderapp.api.Repository

class FragmentPedidoViewModelFactory(private val repository: Repository): ViewModelProvider.Factory {


    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return FragmentPedidoViewModel(repository) as T
    }}