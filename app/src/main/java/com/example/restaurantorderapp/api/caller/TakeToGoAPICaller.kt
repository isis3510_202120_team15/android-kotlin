package com.example.restaurantorderapp.api.caller

import com.example.restaurantorderapp.model.TakeToGoOrder
import retrofit2.Response
import retrofit2.http.*

interface TakeToGoAPICaller {

    /**
     * GET Pedido by Cliente ID
     */
    @GET("pedido/")
    suspend fun getPedidosByClientID(@Query("cliente") id:String): Response<List<TakeToGoOrder>>


    /**
     * POST Pedido
     */
    @POST("pedido")
    suspend fun createPedido(@Body newReserva: TakeToGoOrder)

}