package com.example.restaurantorderapp.api.caller
import com.example.restaurantorderapp.model.Cliente
import com.example.restaurantorderapp.model.MessageHandler
import retrofit2.Response
import retrofit2.http.*

interface ClienteAPICaller {

    /**
     * GET Cliente by Email
     */
    @GET("cliente/")
    suspend fun getClienteByEmail(@Query("email") email:String): Response<Cliente>

    /**
     * POST Cliente
     */
    @POST("cliente")
    suspend fun createCliente(@Body newCliente: Cliente) : Response<Cliente>

    /**
     * DELETE Cliente
     */
    @DELETE("cliente/{id}")
    suspend fun deleteCliente(@Path("id") id:String) : Response<MessageHandler>

}