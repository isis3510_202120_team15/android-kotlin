package com.example.restaurantorderapp.api.caller

import com.example.restaurantorderapp.model.Tarjeta
import retrofit2.Response
import retrofit2.http.*

interface TarjetaAPICaller {

    /**
     * GET ALL Tarjeta from a Cliente
     */
    @GET("cliente/{clienteId}/tarjeta/")
    suspend fun getTarjetas(@Path("clienteId") clienteId:String): Response<List<Tarjeta>>

    /**
     * POST Tarjeta of a Cliente
     */
    @POST("cliente/{clienteId}/tarjeta/")
    suspend fun createTarjeta(@Path("clienteId") clienteId: String,
                    @Body newTarjeta: Tarjeta
    )

}