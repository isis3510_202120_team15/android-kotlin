package com.example.restaurantorderapp.api.instance

import com.example.restaurantorderapp.api.caller.TakeToGoAPICaller
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object TakeToGoOrderAPIInstance {


    private val API by lazy{
        Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    val TakeToGoAPI : TakeToGoAPICaller by lazy {
        API.create(TakeToGoAPICaller::class.java)
    }


}