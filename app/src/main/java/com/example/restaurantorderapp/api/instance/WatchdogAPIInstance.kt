package com.example.restaurantorderapp.api.instance

import com.example.restaurantorderapp.api.caller.WatchdogAPICaller
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object WatchdogAPIInstance {

    private val API by lazy{
        Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    val watchdogAPI : WatchdogAPICaller by lazy {
        API.create(WatchdogAPICaller::class.java)
    }
}