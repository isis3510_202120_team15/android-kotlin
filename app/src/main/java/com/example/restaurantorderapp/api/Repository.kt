package com.example.restaurantorderapp.api

import com.example.restaurantorderapp.api.instance.*
import com.example.restaurantorderapp.model.*
import io.github.reactivecircus.cache4k.Cache
import retrofit2.Response

class Repository {

    private val cacheRestaurante = Cache.Builder().build<String, Response<List<Restaurante>>>()
    private val cacheTTGOrder = Cache.Builder().build<String, Response<List<TakeToGoOrder>>>()
    private val cacheReservas = Cache.Builder().build<String, Response<List<Reserva>>>()
    private val cacheTarjetas = Cache.Builder().build<String, Response<List<Tarjeta>>>()
    private val restaurateKey = "Restaurante"
    private val recomendacionKey = "Recomendacion"
    private val TTGKey = "TTG"
    private val reservaKey = "Reserva"
    private val tarjetaKey = "Tarjeta"

    suspend fun getClienteByEmail(email: String): Response<Cliente> {
        return ClienteAPIInstance.clienteAPI.getClienteByEmail(email)
    }

    suspend fun createCliente(newCliente: Cliente): Response<Cliente> {
        return ClienteAPIInstance.clienteAPI.createCliente(newCliente)
    }

    suspend fun deleteCliente(id: String): Response<MessageHandler> {
        return ClienteAPIInstance.clienteAPI.deleteCliente(id)
    }

    /** **************************************************
     * CRUD calls for Restuarante
     *****************************************************/
    suspend fun getAllRestaurantes(connection: Boolean): Response<List<Restaurante>>? {
        return if (connection) {
            val response = RestauranteAPIInstance.restauranteAPI.getRestaurantes()
            cacheRestaurante.put(restaurateKey, response)
            response
        } else {
            when (
                val cacheResponse = cacheRestaurante.get(restaurateKey)) {
                null -> {
                    null
                }
                else -> {
                    cacheResponse
                }
            }
        }
    }

    suspend fun getRecomendacionesByCliente(
        idCliente: String,
        connection: Boolean,
    ): Response<List<Restaurante>>? {
        return if (connection) {
            val response =
                RestauranteAPIInstance.restauranteAPI.getRecomendacionesForCliente(idCliente)
            cacheRestaurante.put(recomendacionKey, response)
            response
        } else {
            when (
                val cacheResponse = cacheRestaurante.get(restaurateKey)) {
                null -> {
                    null
                }
                else -> {
                    cacheResponse
                }
            }
        }
    }

    /** **************************************************
     * CRUD calls for Tarjeta
     *****************************************************/
    suspend fun getTarjetas(idCliente: String, connected: Boolean): Response<List<Tarjeta>>? {
        return if (connected) {
            val response = TarjetaAPIInstance.tarjetaAPI.getTarjetas(idCliente)
            cacheTarjetas.put(tarjetaKey, response)
            response
        } else {
            when (val response = cacheTarjetas.get(tarjetaKey)) {
                null -> null
                else -> response
            }
        }
    }

    suspend fun createTarjeta(idCliente: String, newTarjeta: Tarjeta) {
        return TarjetaAPIInstance.tarjetaAPI.createTarjeta(idCliente, newTarjeta)
    }

    suspend fun getReservaByClientId(
        idCliente: String,
        connection: Boolean,
    ): Response<List<Reserva>>? {
        return if (connection) {
            val response = ReservaAPIInstance.reservaAPI.getReservasByClientID(idCliente)
            cacheReservas.put(reservaKey, response)
            response
        } else {
            when (val cacheResponse = cacheReservas.get(reservaKey)) {
                null -> {
                    null
                }
                else -> {
                    cacheResponse
                }
            }
        }
    }

    suspend fun createReserva(newReserva: Reserva) {
        return ReservaAPIInstance.reservaAPI.createReserva(newReserva)
    }


    suspend fun getTakeToGoByClientId(
        idCliente: String,
        connected: Boolean,
    ): Response<List<TakeToGoOrder>>? {
        return if (connected) {
            val response = TakeToGoOrderAPIInstance.TakeToGoAPI.getPedidosByClientID(idCliente)
            cacheTTGOrder.put(TTGKey, response)
            response
        } else {
            when (
                val cacheResponse = cacheTTGOrder.get(TTGKey)) {
                null -> {
                    null
                }
                else -> {
                    cacheResponse
                }
            }
        }
    }

    suspend fun createTakeToGoOrder(newOrder: TakeToGoOrder) {
        return TakeToGoOrderAPIInstance.TakeToGoAPI.createPedido(newOrder)
    }

    /*****************************************************
     * CRUD calls for Watchdog
     *****************************************************/
    suspend fun initiateMeasure(measure: Measure, connected: Boolean): Response<TokenId>?{
        return if(connected) WatchdogAPIInstance.watchdogAPI.initiateMeasure(measure)
        else null
    }

    suspend fun endMeasure(id:String?, endHour:Message, connected: Boolean) {
        if(connected && !id.isNullOrEmpty())  WatchdogAPIInstance.watchdogAPI.finishMeasure(id, endHour)
    }

}