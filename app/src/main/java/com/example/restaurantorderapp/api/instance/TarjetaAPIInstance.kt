package com.example.restaurantorderapp.api.instance

import com.example.restaurantorderapp.api.caller.TarjetaAPICaller
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Singleton Class for managing client API calls
 */
object TarjetaAPIInstance {

    private val API by lazy{
        Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    val tarjetaAPI : TarjetaAPICaller by lazy {
        API.create(TarjetaAPICaller::class.java)
    }

}