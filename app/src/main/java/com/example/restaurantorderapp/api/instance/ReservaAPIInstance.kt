package com.example.restaurantorderapp.api.instance
import com.example.restaurantorderapp.api.caller.ReservaAPICaller
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Singleton Class for managing Reserva API calls
 */
object ReservaAPIInstance {

    private val API by lazy{
        Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    val reservaAPI : ReservaAPICaller by lazy {
        API.create(ReservaAPICaller::class.java)
    }

}