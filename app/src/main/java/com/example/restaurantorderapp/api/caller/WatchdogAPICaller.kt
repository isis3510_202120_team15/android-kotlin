package com.example.restaurantorderapp.api.caller

import com.example.restaurantorderapp.model.Measure
import com.example.restaurantorderapp.model.Message
import com.example.restaurantorderapp.model.TokenId
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.Path

interface WatchdogAPICaller {

    @POST("measure")
    suspend fun initiateMeasure(@Body initmeasure:Measure) : Response<TokenId>


    @POST("measure/{tokenId}")
    suspend fun finishMeasure(@Path("tokenId") id:String,@Body endHour:Message)

}