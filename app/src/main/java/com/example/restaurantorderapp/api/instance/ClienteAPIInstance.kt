package com.example.restaurantorderapp.api.instance

import com.example.restaurantorderapp.api.caller.ClienteAPICaller
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Singleton Class for managing client API calls
 */

object ClienteAPIInstance {

    private val API by lazy{
        Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    val clienteAPI : ClienteAPICaller by lazy {
        API.create(ClienteAPICaller::class.java)
    }

}