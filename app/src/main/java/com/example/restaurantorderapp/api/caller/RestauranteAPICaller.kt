package com.example.restaurantorderapp.api.caller
import com.example.restaurantorderapp.model.Restaurante
import retrofit2.Response
import retrofit2.http.*

interface RestauranteAPICaller {


    @GET("restaurante")
    suspend fun getRestaurantes(): Response<List<Restaurante>>

    @GET("recomendaciones/{idCliente}")
    suspend fun getRecomendacionesForCliente(@Path("idCliente") idCliente:String):Response<List<Restaurante>>

}