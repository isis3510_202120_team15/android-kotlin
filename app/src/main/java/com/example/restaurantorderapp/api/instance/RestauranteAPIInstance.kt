package com.example.restaurantorderapp.api.instance

import com.example.restaurantorderapp.api.caller.RestauranteAPICaller
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Singleton Class for managing Restaurant API calls
 */
object RestauranteAPIInstance {

    private val API by lazy {
        Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    val restauranteAPI : RestauranteAPICaller by lazy {
        API.create(RestauranteAPICaller::class.java)
    }
}
