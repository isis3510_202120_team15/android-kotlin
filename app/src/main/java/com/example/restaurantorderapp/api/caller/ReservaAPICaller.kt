package com.example.restaurantorderapp.api.caller
import com.example.restaurantorderapp.model.Reserva
import retrofit2.Response
import retrofit2.http.*

interface ReservaAPICaller {

    /**
     * GET Reserva by Cliente ID
     */
    @GET("reserva/")
    suspend fun getReservasByClientID(@Query("cliente") id:String): Response<List<Reserva>>


    /**
     * POST Reserva
     */
    @POST("reserva")
    suspend fun createReserva(@Body newReserva: Reserva)

}